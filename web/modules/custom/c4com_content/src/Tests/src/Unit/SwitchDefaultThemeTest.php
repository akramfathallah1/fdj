<?php

namespace Drupal\c4com_content\Tests;

Use Drupal\Tests\UnitTestCase;
use Drupal\Tests\BrowserTestBase;
use Drupal\simpletest\WebTestBase;

/**
 * TODO.
 *
 * @group c4com_content
 */
class SwitchDefaultThemeTest
  extends BrowserTestBase
  //extends WebTestBase
  {

  /**
   * Use the Standard profile, so that there are profile config overrides.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'config',
    'config_update',
    'config_update_ui',
  ];

  /**
   * The admin user that will be created.
   *
   * @var object
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  /*protected function setUp() {
    parent::setUp();

    // Create user and log in.
    $this->adminUser = $this->createUser([
      'access administration pages',
      'administer themes',
      'view config updates report',
      'synchronize configuration',
      'export configuration',
      'import configuration',
      'revert configuration',
      'delete configuration',
    ]);
    $this->drupalLogin($this->adminUser);
  }*/

  /**
   * TODO.
   */
  public function testDefaultTheme() {

    /*$this->drupalGet('admin/config/development/configuration/report/type/system.simple');
    $session = $this->assertSession();
    $session->responseNotContains('system.theme');*/

    // TODO.
    /*$theme_handler = $this->getMockBuilder('Drupal\c4com_content\Services\C4comTheme')
      ->disableOriginalConstructor()
      ->getMock();


    $this->drupalGet('admin/config/development/configuration/single/export/system.simple/system.theme');
    $session = $this->assertSession();
    $session->pageTextContains('admin: seven');
    $session->pageTextContains('default: c4com');


    /*
    // Install and enable c4com theme.
    $theme_handler->setTheme('c4com');
    $test = 'aemen ok';
    $tet = \Drupal::configFactory()->config('system.theme')->get('default');
    var_dump($tet);die;
    //$this->assertEquals(1, 1);
    //$this->assertEquals(\Drupal::configFactory()->config('system.theme')->get('default'), 'bartik');

    // Install Bartik and set it as the default theme.
    /*$theme_handler->setTheme(['bartik']);
    $this->drupalGet('admin/appearance');
    $this->clickLink(t('Set as default'));
    $this->assertEqual($this->config('system.theme')->get('default'), 'bartik');

    // Test the default theme on the secondary links (blocks admin page).
    $this->drupalGet('admin/structure/block');
    $this->assertText('Bartik(' . t('active tab') . ')');

    // Switch back to Stark and test again to test that menu cache is cleared.
    $this->drupalGet('admin/appearance');

    // Stark is the first 'Set as default' link.
    $this->clickLink(t('Set as default'));
    $this->drupalGet('admin/structure/block');*/
    //$this->assertText('C4COM(' . t('active tab') . ')');
  }

}
