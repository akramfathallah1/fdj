<?php

namespace Drupal\c4com_content\Twig_extension;

use Drupal\block\Entity\Block;

/**
 * Twig extension class to render block.
 */
class TwigGetBlock extends \Twig_Extension {

  /**
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'block_display';
  }

  /**
   * In this function we can declare the extension function.
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('displayBlock',
        [$this, 'displayBlock'],
        [
          'is_safe' => ['html'],
        ]
      ),
    ];
  }

  /**
   * The php function to load a given block.
   */
  public function displayBlock($block_id) {
    $block = Block::load($block_id);
    // TODO.
    if ($block) {
      return \Drupal::entityManager()->getViewBuilder('block')->view($block);
    }
  }

}
