<?php

namespace Drupal\c4com_content\Twig_extension;

/**
 * Change byte's FileSize to K Octets.
 */
class TwigFileSize extends \Twig_Extension {

  /**
   * In this function we can declare the extension function.
   */
  public function getFilters() {
    return [new \Twig_SimpleFilter('filesize', [$this, 'fileSize'])];
  }

  /**
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'c4com_content.filesize';
  }

  /**
   * Change byte's FileSize to K Octets.
   *
   * @param string $bytes
   *   Receive file's size in bytes.
   *
   * @return string
   *   Return file's size in Kilo Octets.
   */
  public function fileSize($bytes) {
    return round($bytes / 1000) . ' KB';
  }

}
