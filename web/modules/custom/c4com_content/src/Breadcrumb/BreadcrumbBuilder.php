<?php

namespace Drupal\c4com_content\Breadcrumb;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class to implement hook insert.
 */
class BreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $parameters = $route_match->getParameters()->all();
    if (isset($parameters['node'])) {
      $type = $parameters['node']->getType();
      // Return TRUE to customize if one of this types:
      return ($type === 'landing_page' || $type === 'actu_mag' || $type === 'channel_page' || $type === 'home_page');
    }

    return NULL;
  }

  /**
   * Build custom breadcrumb.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return object
   *   The breadcrumb.
   */
  public function build(RouteMatchInterface $route_match) {
    // Initiate breadcrumb.
    $breadcrumb = new Breadcrumb();

    // Initiate params.
    $parameters = $route_match->getParameters()->all();
    // Populate breadcrumb only for Landing Pages for now.
    if (isset($parameters['node']) && $parameters['node']->getType() === 'landing_page') {
      $n_x = FALSE;
      $breadcrumb->addCacheContexts(['url']);

      // Get current page term.
      $current = $parameters['node']->get('field_menu_paramaters');

      // Load current page parents.
      $ancestors = $this->entityTypeManager->getStorage('taxonomy_term')->loadAllParents($current->target_id);
      // Clean $ancestors: remove term it self.
      unset($ancestors[$current->target_id]);

      // Check if term is a parent or not.
      if (!empty($ancestors)) {
        // N-1 case.
        if (count($ancestors) == 1) {
          $ancestor = reset($ancestors);
          $this->populateBreadcrumb($ancestor, $breadcrumb);
          $this->getActualPage($breadcrumb);
        }
        else {
          // N-x case.
          $n_x = TRUE;
          $ancestor = $this->entityTypeManager->getStorage('taxonomy_term')
            ->loadParents($current->target_id);
          $this->populateBreadcrumb(reset($ancestor), $breadcrumb, $n_x);
          $this->getActualPage($breadcrumb);
        }
        // Load siblings. Exception: N-x terms.
        if (!empty($ancestor) && !$n_x) {
          $this->loadChildren($ancestor->tid->getValue()[0]['value'], $current->target_id, $breadcrumb);
        }
      }
      else {
        $this->getActualPage($breadcrumb);
        // Add 'Access to pages' link only if term is not orphan.
        if (!empty($this->entityTypeManager->getStorage('taxonomy_term')->loadChildren($current->target_id))) {
          $breadcrumb->addLink(Link::createFromRoute(t('Access to pages'), '<none>'));
        }
        $this->loadChildren($current->target_id, $current->target_id, $breadcrumb);
      }
    }

    return $breadcrumb;
  }

  /**
   * Add children terms to the breadcrumb.
   *
   * @param int $parent_tid
   *   Parent taxonomy tid.
   * @param int $current_tid
   *   Current taxonomy page tid.
   * @param object $breadcrumb
   *   The breadcrumb object.
   * @param bool $n_x
   *   True if term is a child level N-x > N-1.
   *
   * @return array
   *   Children of the term.
   */
  public function loadChildren($parent_tid, $current_tid, &$breadcrumb, $n_x = FALSE) {
    $children = $this->entityTypeManager->getStorage('taxonomy_term')->loadChildren($parent_tid);
    // Remove term it self from children list.
    unset($children[$current_tid]);
    // Add every child in breadcrumb.
    foreach ($children as $child) {
      $this->populateBreadcrumb($child, $breadcrumb, $n_x);
    }

    return $children;
  }

  /**
   * Add Actual page to the breadcrumb, without a link.
   *
   * @param object $breadcrumb
   *   The breadcrumb object.
   *
   * @return object
   *   The breadcrumb.
   */
  public function getActualPage(&$breadcrumb) {
    // Add Actual page, without a link.
    $request = \Drupal::request();
    $route_match = \Drupal::routeMatch();
    $page_title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());

    if (!empty($page_title)) {
      $breadcrumb->addLink(Link::createFromRoute($page_title, '<none>'));
    }

    return $breadcrumb;
  }

  /**
   * Populate breadcrumb with custom elements.
   *
   * @param int $term
   *   Parent taxonomy tid.
   * @param object $breadcrumb
   *   The breadcrumb object.
   * @param bool $n_x
   *   True if term is a child level N-x > N-1.
   *
   * @return object
   *   The breadcrumb.
   */
  public function populateBreadcrumb($term, &$breadcrumb, $n_x = FALSE) {
    // Create route with params for breadcrumb.
    if (!empty($term->field_url_menu->getValue()[0])) {
      $url = Url::fromUri($term->field_url_menu->getValue()[0]['uri']);
      $params = $url->getRouteParameters();
      if (isset($params['node'])) {
        $element = [
          'name' => $n_x === FALSE ? $term->getName() : '<',
          'route_name' => $url->getRouteName(),
          'route_params' => ['node' => $params['node']],
        ];
        $breadcrumb->addLink(Link::createFromRoute($element['name'], $element['route_name'], $element['route_params']));
      }
    }
    return $breadcrumb;
  }

}
