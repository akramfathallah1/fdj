<?php

namespace Drupal\c4com_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\c4com_content\Services\RssService;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Component\Utility\Unicode;

/**
 * RSS Controller.
 */
class RssController extends ControllerBase {

  /**
   * The RSS service.
   *
   * @var \Drupal\C4com_content\Services\RssService
   */
  private $RssService;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(RssService $rss_service, EntityTypeManager $entityTypeManager) {
    $this->RssService = $rss_service;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Static funtion (DI)
   */
  public static function create(ContainerInterface $container) {
    $rss_service = $container->get('c4com_content.rss.service');
    $entity_manager = $container->get('entity_type.manager');
    return new static($rss_service, $entity_manager);
  }

  /**
   * Global function actuality RSS.
   */
  public function contentActualitiesRss() {
    $teaser = '';
    $url_img_tablette = '';
    $url_img_mobile = '';
    $output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <channel>
      <title><![CDATA[actualites]]> </title>
      <link>http://www.carrefour.com/fr/rss/actualites</link>
      <language>fr</language>";

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('type', 'actu_mag')
      ->condition('field_type', 'actuality')
      ->condition('field_xml_data', 1)
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    foreach ($nodes as $key => $val) {
      if ($val->hasTranslation('fr')) {
        $output .= '<items><item>';
        $value = $val->getTranslation('fr');
        $options = ['absolute' => TRUE];
        $url = Url::fromRoute('entity.node.canonical', ['node' => $value->id()], $options);
        $url = $url->toString();
        $media_entity = $value->get('field_picture_head')->first()->get('entity')->getTarget()->get('field_image_image')->first()->get('entity')->getTarget()->getValue();
        $img_desktop = File::load($media_entity->field_media_image->target_id);
        $url_img_desktop = $img_desktop->url();
        if (!empty($media_entity->field_image_mobile->target_id)) {
          $img_mobile = File::load($media_entity->field_image_mobile->target_id);
          $url_img_mobile = $img_mobile->url();
        }
        if (!empty($media_entity->field_image_tablette->target_id)) {
          $img_tablette = File::load($media_entity->field_image_tablette->target_id);
          $url_img_tablette = $img_tablette->url();
        }
        if ($value->body->getValue()) {
          $teaser = $value->body->getValue()[0]['value'];
        }
        $output .= '<title_actuality><![CDATA[' . $value->getTitle() . ']]></title_actuality><description_actuality><![CDATA['
        . $teaser . ']]></description_actuality><url_actuality>'
        . $url . '</url_actuality><date_creation>'
        . date('d/m/Y', $value->getCreatedTime()) . '</date_creation><img_desktop>'
        . $url_img_desktop . '</img_desktop><img_tablette>'
        . $url_img_tablette . '</img_tablette><img_mobile>'
        . $url_img_mobile . '</img_mobile>';

      }
      // Get content field xml.
      $output .= $this->RssService->getContentActuality($value->get('field_content')->getValue());
      $query = \Drupal::entityTypeManager()->getStorage('node')->getQuery();
      $nids = $query->condition('type', 'document')
        ->condition('field_article_url', $value->id())

        ->condition('status', 1)
        ->sort('created', 'DESC')
        ->execute();
      $output .= $this->RssService->getDocumentActuality($nids);

      $output .= '</item></items>';
    }
    $output .= '</channel>';
    $output = str_replace("&nbsp;", " ", $output);

    if ($encoding = (Unicode::encodingFromBOM($output))) {
      $output = mb_substr(Unicode::convertToUtf8($output, $encoding), 1);

    }
    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml');
    $response->setContent($output);
    return $response;

  }

  /**
   * XML doc CP.
   */
  public function contentCpRss() {
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('type', 'document')
      ->condition('field_xml_data', 1)
      ->condition('field_tag_paramaters.entity.field_tag_type.entity.tid', '1')
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->execute();
    $output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <channel>
      <title><![CDATA[Communiqué de presse]]></title>
      <link>http://www.carrefour.com/fr/rss/communiques</link>
      <language>fr</language>";
    $output .= $this->RssService->getDocumentActuality($nids);
    $output .= '</channel>';
    $output = str_replace("&nbsp;", " ", $output);
    if ($encoding = (Unicode::encodingFromBOM($output))) {
      $output = mb_substr(Unicode::convertToUtf8($output, $encoding), 1);

    }
    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml');
    $response->setContent($output);
    return $response;
  }

  /**
   * XML doc publication.
   */
  public function contentPublicationRss() {

    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('type', 'document')
      ->condition('field_xml_data', 1)
      ->condition('field_tag_paramaters.entity.field_tag_type.entity.tid', '2')
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->execute();
    $output = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
      <channel>
      <title><![CDATA[Documents type puplication]]> </title>
      <link>http://www.carrefour.com/fr/rss/publication</link>
      <language>fr</language>";
    $output .= $this->RssService->getDocumentActuality($nids);
    $output .= '</channel>';
    $output = str_replace("&nbsp;", " ", $output);
    if ($encoding = (Unicode::encodingFromBOM($output))) {
      $output = mb_substr(Unicode::convertToUtf8($output, $encoding), 1);

    }
    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml');
    $response->setContent($output);
    return $response;
  }

}
