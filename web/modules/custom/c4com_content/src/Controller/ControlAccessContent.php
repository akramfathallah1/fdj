<?php

namespace Drupal\c4com_content\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Controller\NodeViewController;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * ControlAccessContent controller.
 */
class ControlAccessContent extends NodeViewController {

  /**
   * View node control.
   */
  public function view(EntityInterface $node, $view_mode = 'default', $langcode = NULL) {
    // Authorized public pages.
    $content_anonymous_view = [
      'actu_mag',
      'landing_page',
      'channel_page',
      'home_page',
    ];
    if (\Drupal::currentUser()->isAnonymous()) {
      if (in_array($node->getType(), $content_anonymous_view)) {
        return parent::view($node, $view_mode, $langcode);
      }
      else {
        /* 403
        throw new AccessDeniedHttpException();
        404
        throw new NotFoundHttpException();
        redirection 404 */
        $url = Url::fromRoute('system.404');
        return new RedirectResponse($url->toString());
      }
    }
    else {
      return parent::view($node, $view_mode, $langcode);

    }
  }

}
