<?php

namespace Drupal\c4com_content\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class WidgetFormAlterSubscriber.
 */
class WidgetFormAlterSubscriber implements EventSubscriberInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::WIDGET_FORM_ALTER => 'hookFieldWidgetFormAlter',
    ];
  }

  /**
   * Altering form array from here.
   */
  public function hookFieldWidgetFormAlter($event) {
    if (get_class($event->getContext()['widget']) == "Drupal\link_attributes\Plugin\Field\FieldWidget\LinkWithAttributesWidget") {
      $element = $event->getElement();
      $fieldDefinitions = $event->getContext()['items']->getFieldDefinition();
      $configs = $fieldDefinitions->getThirdPartySetting('link autocomplete select_select_types', 'allowed_content_types');
      $settings = is_array($configs) ? $configs : [];
      $bundles = array_filter($settings);
      if ($bundles) {
        $element['uri']['#selection_settings'] = [
          'target_bundles' => $bundles ,
        ];
      }
      $event->setElement($element);
    }
  }

}
