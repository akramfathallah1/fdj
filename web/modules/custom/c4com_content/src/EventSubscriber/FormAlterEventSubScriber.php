<?php

namespace Drupal\c4com_content\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class FormAlterEventSubScriber.
 */
class FormAlterEventSubScriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   */
  public function __construct(LanguageManagerInterface $language_manager, EntityTypeManager $entityTypeManager) {
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'hookFormAlter',
    ];
  }

  /**
   * Altering form array from here.
   */
  public function hookFormAlter($event) {
    $language = $this->languageManager->getCurrentLanguage()->getId();
    // TODO: switch case.
    $form_id = $event->getFormId();
    if (!isset($_GET['ajax_form'])) {
      // Custom validation for color palette.
      if ($form_id == 'taxonomy_term_palette_couleur_form') {
        $form = $event->getForm();
        $form['#validate'][] = [$this, 'validateFormPalette'];
        $event->setForm($form);
      }

      // Custom Plugin date config.
      if ($form_id === 'node_card_agenda_form' && $language === 'fr') {
        unset($event->getForm()['field_date_event']['widget'][0]['value']['#date_part_order'][5]);
      }

      // Custom validation for IEF block cards.
      if ($form_id == 'node_bloc_cards_evenement_recto_form') {
        $form = $event->getForm();
        $form['#validate'][] = [$this, 'validateFormBlockCard'];
        $event->setForm($form);
      }

      // Custom validation add domain block "verify domain field".
      if ($form_id == 'node_block_domaine_form' || $form_id == 'node_block_domaine_edit_form') {
        $form = $event->getForm();
        $form['#validate'][] = [$this, 'validateFormDomain'];
        $event->setForm($form);
      }

      // Custom validation for IEF block cards.
      if ($form_id == 'views_exposed_form') {
        $form = $event->getForm();
        if (!empty($form['field_year_value'])) {
          $form['field_year_value']['#type'] = 'select';
          $form['field_year_value']['#size'] = 0;
          $form['field_year_value']['#options'] = $this->getYears();
          $form['field_year_value']['#default_value'] = '_none';
          $form['field_year_value']['#empty_option'] = $this->t('- Select year -');
          $form['field_year_value']['#empty_value'] = '';
          $event->setForm($form);
        }
      }

      if ($form_id == 'node_module_banniere_form' || $form_id == 'node_module_banniere_edit_form') {
        $form = $event->getForm();
        $form['field_bloc_contenu']['widget']['#states'] = [
          'visible' => [
            [
              [':input[name="field_choose_block_type"]' => ['value' => 'block_content']],
            ],
          ],
        ];
        $event->setForm($form);
      }

      // Form node Mag/Actu.
      if ($form_id == 'node_actu_mag_form' || $form_id == 'node_actu_mag_edit_form') {
        $form = $event->getForm();
        $form['field_temps_de_lecture']['widget'][0]['value']['#states'] = [
          'visible' => [
            [
              [':input[name="field_type"]' => ['value' => 'magazine']],
            ],
          ],
        ];
        $form['field_proposal_article_mag']['#states'] = [
          'visible' => [
            [
              [':input[name="field_type"]' => ['value' => 'magazine']],
            ],
          ],
        ];
        $event->setForm($form);
      }

      // Form node channel page.
      if ($form_id == 'node_channel_page_form' || $form_id == 'node_channel_page_edit_form') {
        $form = $event->getForm();
        $form['#validate'][] = [$this, 'validateFormChannelPage'];
        $event->setForm($form);
      }

      // Form node Homepage.
      if ($form_id == 'node_home_page_form' || $form_id == 'node_home_page_edit_form') {
        $form = $event->getForm();
        $form['#validate'][] = [$this, 'validateFormPageHome'];
        $event->setForm($form);
      }
      // Form config field link.
      if ($form_id == 'field_config_edit_form') {
        $form = $event->getForm();
        $form_state = $event->getFormState();
        $entity = $form_state->getFormObject()->getEntity();
        $filed_type = $entity->get('field_type');
        if ($filed_type === "link") {
          $content_types = $this->entityTypeManager
            ->getStorage('node_type')
            ->loadMultiple();
          foreach ($content_types as $node_type) {
            $options[$node_type->id()] = $node_type->label();
          }
          $form['third_party_settings']['link autocomplete select_select_types'] = [
            '#type' => 'fieldset',
            '#collapsible' => FALSE,
            '#title' => $this->t('Autocomplete select content types'),
          ];
          $settings = $entity->getThirdPartySetting('link autocomplete select_select_types', 'allowed_content_types');
          $default = is_array($settings) ? $settings : [];
          $form['third_party_settings']['link autocomplete select_select_types']['allowed_content_types'] = [
            '#type' => 'checkboxes',
            '#title' => t('Allowed content types for internal links'),
            '#description' => t('Check allowed content types. If none are checked, then all are allowed.'),
            '#options' => $options,
            '#default_value' => $default,
          ];
        }
        $event->setForm($form);
      }
      // Form node Homepage.
      if ($form_id == 'search_form' || $form_id == 'search_block_form') {
        $form = $event->getForm();
        $url = \Drupal\Core\Url::fromRoute('search.view_c4com');
        // make the redirection
        $form['#action'] = $url->toString();
        unset($form['advanced']);
        unset($form['help_link']);
        $event->setForm($form);
      }
    }
  }

  /**
   * Validate Form taxonomy color palette function.
   */
  public function validateFormPalette(array &$form, FormStateInterface $form_state) {
    $hexadecimal = $form_state->getValue('field_code_hexadecimal')[0]['value'];
    if (!preg_match('/^#[a-f0-9]{6}$/i', $hexadecimal)) {
      $form_state->setErrorByName('field_code_hexadecimal', $this->t('The hexadecimal code field has not the right good color hexadecimal format. (Example: #RRGGBB).'));
    }
  }

  /**
   * Validate Form bloc cards function.
   */
  public function validateFormBlockCard(array &$form, FormStateInterface $form_state) {
    // Get the array key.
    $key = key($form_state->get('inline_entity_form'));
    $i = 0;
    $inline_entities = [];

    // Loop all entities in inline form.
    foreach ($form_state->get('inline_entity_form')[$key]['entities'] as $delta => $entity_item) {
      if (!empty($entity_item['entity'])) {
        $entity_type = $entity_item['entity']->getType();
        if (!empty($entity_type)) {
          $i++;
          $inline_entities[$entity_type][$delta] = $entity_item['entity'];
        }
      }
    }

    // Contribute at least 3 cards.
    if ($i < 3) {
      $form_state->setErrorByName('field_cards', $this->t('You must enter at least 3 cards'));
    }
    // Check if there is different type of cards.
    elseif (count($inline_entities) > 1) {
      $form_state->setErrorByName('field_cards', $this->t("The field CARDS don't contain elements of same type"));
    }
  }

  /**
   * Function get years for field year in exposed filter.
   */
  public function getYears() {
    $current_year = date('Y');
    $begin_year = $current_year - 10;
    for ($begin_year; $begin_year <= $current_year; $begin_year++) {
      $years[$begin_year] = $begin_year;
    }
    return $years;
  }

  /**
   * Validate Form bloc cards function.
   */
  public function validateFormDomain(array &$form, FormStateInterface $form_state) {
    // Get the domain value field.
    if ($form_state->getValue('field_domaine')[0]) {
      $tid = $form_state->getValue('field_domaine')[0]['target_id'];
      // TODO: dependency injection.
      $parents = \Drupal::service('entity_type.manager')
        ->getStorage('taxonomy_term')
        ->loadParents($tid);
      if ($parents) {
        $form_state->setErrorByName('field_domaine', t('Verify field domain value'));
      }
    }
  }

  /**
   * Validate Form Home page form function.
   */
  public function validateFormPageHome(array &$form, FormStateInterface $form_state) {
    $list = [];
    foreach ($form_state->getValues() as $key => $value) {
      // Loop all field article.
      if (preg_match('/^field_bloc_magazine/', $key)
        // TODO && !is_null($form_state->getValue($key)[0]['subform']['field_article_magazine'][0]['target_id']).
      ) {
        // Get entity id.
        $nid = $form_state->getValue($key)[0]['subform']['field_article_magazine'][0]['target_id'];
        // Load entity.
        $node = $this->entityTypeManager->getStorage('node')->load($nid);
        // Get type article.
        if (!empty($node)) {
          $type = $node->get('field_type')->getValue()[0]['value'];
          // Verify if article type magazine.
          if ($type != 'magazine') {
            $form_state->setErrorByName($key . "][0][subform][field_article_magazine][0",
              $this->t('Articles list contains actualities. Please make sure to only choose magazines'));
          }
          elseif (in_array($nid, $list)) {
            $form_state->setErrorByName($key . "][0][subform][field_article_magazine][0",
              $this->t('Articles list contains duplicates magazines'));
          }
          else {
            array_push($list, $nid);
          }
        }
      }

    }
  }

  /**
   * Validate Form Channel page form function.
   */
  public function validateFormChannelPage(array &$form, FormStateInterface $form_state) {
    $nid = "";
    $tag_channel_banner_article = "";
    if ($form_state->getFormObject()) {
      $nid = $form_state->getformObject()->getEntity()->id();
    }
    // Get channel page thematic.
    $current_page_channel_thematic = $form_state->getValue('field_thematic')[0]['target_id'];
    $exist = $this->verifyChannelPageExist($current_page_channel_thematic, $nid);
    if ($exist) {
      $form_state->setErrorByName("field_thematic", t('Channel selected already has a page'));
    }
    elseif ($form_state->getValue('field_mag_article')[0]) {
      // Get article banner id.
      $nid_banner = $form_state->getValue('field_mag_article')[0]['target_id'];
      // Load entity.
      $node = $this->entityTypeManager->getStorage('node')->load($nid_banner);
      // Get type article.
      $type = $node->get("field_type")->getValue()[0]['value'];
      // Get paragraph tag contenu id for article banner.
      $paragraph_tag_id = $node->field_tags_mag_actu->getValue()[0]['target_id'];
      // Load paragraph entity.
      $pragraph_content = $this->entityTypeManager->getStorage('paragraph')->load($paragraph_tag_id);
      if ($pragraph_content->field_tag_channel->getValue()) {
        $tag_channel_banner_article = $pragraph_content->field_tag_channel->getValue()[0]['target_id'];
      }
      // Verify if article type magazine.
      if ($type != "magazine") {
        $form_state->setErrorByName("field_mag_article", t('Header contains actualities. Please make sure to only choose magazines'));
      }
      elseif ($current_page_channel_thematic != $tag_channel_banner_article) {
        $form_state->setErrorByName("field_mag_article", t('Channel and the Header article do not have the same thematic'));
        $form_state->setErrorByName("field_thematic", t('Channel and the Header article do not have the same thematic'));
      }
    }
  }

  /**
   * Page channel exist !.
   */
  public function verifyChannelPageExist($id, $nid) {
    $exist = TRUE;
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('type', 'channel_page')
      ->condition('field_thematic.entity.tid', $id)
      ->execute();
    if (!count($nids)) {
      $exist = FALSE;
    }
    elseif (in_array($nid, $nids)) {
      $exist = FALSE;
    }
    return $exist;
  }

}
