<?php

namespace Drupal\c4com_content\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Class to implement hook insert.
 */
class EntityInsertEventSubScriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::ENTITY_INSERT => 'hookEntityInsert',
    ];
  }

  /**
   * Implement hook entity insert event subscriber.
   */
  public function hookEntityInsert($event) {
    $entityType = $event->getEntity()->getEntityTypeId();
    if ($entityType === 'node') {
      if ($event->getEntity()->getType() === 'landing_page') {
        if ($event->getEntity()->get('field_menu_paramaters')->getValue()) {
          $termId = $event->getEntity()
            ->get('field_menu_paramaters')
            ->getValue()[0]['target_id'];
          $termMenu = Term::load($termId);
          $termMenu->set(
            'field_url_menu',
            [
              'uri' => "entity:node/" . $event->getEntity()->id(),
              'options' => [
                'attributes' =>
                [
                  'target' => '_self',
                ],
              ],
            ]
          );
          $termMenu->Save();
        }
      }
      elseif ($event->getEntity()->getType() === 'channel_page') {
        if ($event->getEntity()->get('field_thematic')->getValue()) {
          $termId = $event->getEntity()
            ->get('field_thematic')
            ->getValue()[0]['target_id'];
          $termMenu = Term::load($termId);
          $termMenu->set(
          'field_url_menu',
          [
            'uri' => "entity:node/" . $event->getEntity()->id(),
            'options' => [
              'attributes' => [
                'target' => '_self',
              ],
            ],
          ]
          );
          $termMenu->Save();
        }
      }
    }
  }

}
