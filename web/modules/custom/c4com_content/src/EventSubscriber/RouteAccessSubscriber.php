<?php

namespace Drupal\c4com_content\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * RouteAccessSubscriber class.
 */
class RouteAccessSubscriber extends RouteSubscriberBase {

  /**
   * Route alter for nodes.
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.node.canonical')) {
      $route->setDefault('_controller', '\Drupal\c4com_content\Controller\ControlAccessContent::view');
    }
  }

}
