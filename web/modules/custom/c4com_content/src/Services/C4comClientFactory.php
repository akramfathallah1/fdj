<?php

namespace Drupal\c4com_content\Services;

use GuzzleHttp\Client;

/**
 * GET webservice content.
 */
class C4comClientFactory {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Class constructor.
   *
   * @param \GuzzleHttp\Client $httpClient
   *   The HTTP client.
   */
  public function __construct(Client $httpClient) {
    $this->httpClient = $httpClient;
  }

  /**
   * Function get data from ws.
   *
   * @var string
   *   Webservice URL.
   *
   * @return string
   *   Result of webservice.
   */
  public function get($url) {
    $result = NULL;
    try {
      $result = $this->httpClient->request('GET', $url)->getBody()->getContents();
    }
    catch (\Exception $error) {
      // TODO: inject logger.
      $logger = \Drupal::logger('Symex webservice error');
      $logger->error($error->getMessage());
    }
    return $result;

  }

}
