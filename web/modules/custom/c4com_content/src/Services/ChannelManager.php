<?php

namespace Drupal\c4com_content\Services;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\file\Entity\File;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Channel Manager Class.
 */
class ChannelManager {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;

  }

  /**
   * Get Article By channel.
   */
  public function getArticleByChannel($id_channel, $id_mag_banner = "") {
    $node_list = [];
    $url_img_mobile = "";
    $url_img_tablette = "";
    $teaser = "";
    $title = [];
    $language = $this->languageManager->getCurrentLanguage()->getId();
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $nids = $query->condition('type', 'actu_mag')
      ->condition('field_type', 'magazine')
      ->condition('field_tags_mag_actu.entity.field_tag_channel.entity.tid', $id_channel)
      ->condition('nid', $id_mag_banner, '!=')
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->execute();
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    foreach ($nodes as $key => $val) {
      if ($val->hasTranslation($language)) {
        $value = $val->getTranslation($language);
        $media_entity = $value->get('field_picture_head')->first()->get('entity')->getTarget()->get('field_image_image')->first()->get('entity')->getTarget()->getValue();
        $img_desktop = File::load($media_entity->field_media_image->target_id);
        $url_img_desktop = $img_desktop->url();
        if (!empty($media_entity->field_image_mobile->target_id)) {
          $img_mobile = File::load($media_entity->field_image_mobile->target_id);
          $url_img_mobile = $img_mobile->url();
        }
        if (!empty($media_entity->field_image_tablette->target_id)) {
          $img_tablette = File::load($media_entity->field_image_tablette->target_id);
          $url_img_tablette = $img_tablette->url();
        }
        if ($value->body->getValue()) {
          $teaser = $value->body->getValue()[0]['value'];
        }
        if ($value->field_title_head->getValue()) {
          $title = $value->field_title_head->getValue();
        }
        $node_list[$key] = [
          'title' => $title,
          'teaser' => $teaser,
          'label_link' => 'Read article',
          'url_article' => $value->toUrl()->toString(),
          'img_desktop' => $url_img_desktop,
          'img_tablette' => $url_img_tablette,
          'img_mobile' => $url_img_mobile,
        ];
      }
    }
    return json_encode($node_list);

  }

}
