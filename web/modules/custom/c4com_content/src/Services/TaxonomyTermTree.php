<?php

namespace Drupal\c4com_content\Services;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\file\Entity\File;

/**
 * GET taxonomy terms menu in a tree.
 */
class TaxonomyTermTree {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager, LanguageManagerInterface $language_manager, EntityRepositoryInterface $entity_repository) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;
    $this->entityRepository = $entity_repository;
  }

  /**
   * Loads the tree of a vocabulary.
   */
  public function load($vocabulary) {
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary);
    $tree = [];
    foreach ($terms as $tree_object) {
      $this->buildTree($tree, $tree_object, $vocabulary);
    }
    return $tree;
  }

  /**
   * Populates a tree array given a taxonomy term tree object.
   */
  protected function buildTree(&$tree, $object, $vocabulary) {
    $language = $this->languageManager->getCurrentLanguage()->getId();
    if ($object->depth != 0) {
      return;
    }

    // Load term in the current language.
    $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($object->tid);
    $term_data = $this->entityRepository->getTranslationFromContext($term, $language);
    $tree[$object->tid] = (object) ['name' => $term_data->name->value];
    if (!empty($term_data->field_url_menu) && !empty($term_data->field_url_menu->getValue()[0])) {
      // Convert the URI into URL.
      $tree[$object->tid]->url = Url::fromUri($term_data->field_url_menu->getValue()[0]['uri'])->toString();
      // Add targets if it exists.
      if (isset($term_data->field_url_menu->getValue()[0]['options']['attributes']['target'])) {
        $tree[$object->tid]->open_link = $term_data->field_url_menu->getValue()[0]['options']['attributes']['target'];
      }
      else {
        $tree[$object->tid]->open_link = '_self';
      }
      // Add category description only for parent item.
      if (isset($term_data->field_category_summary)) {
        if (isset($term_data->field_category_summary->getValue()[0]) && !$object->parents[0]) {
          $tree[$object->tid]->category_summary = $term_data->field_category_summary->getValue()[0]['value'];
        }
      }
      // Add category link label only for parent item.
      if (isset($term_data->field_link_label)) {
        if (isset($term_data->field_link_label->getValue()[0]) && !$object->parents[0]) {
          $tree[$object->tid]->link_label = $term_data->field_link_label->getValue()[0]['value'];
        }
        else {
          $tree[$object->tid]->link_label = t('The essential');
        }
      }
      // Add color channel only for taxonomy channel items.
      if (isset($term_data->field_color_channel)) {
        if (isset($term_data->field_color_channel->getValue()[0])) {
          $color_tid = $term_data->field_color_channel->getValue()[0]['target_id'];
          $color_property = $this->entityTypeManager->getStorage('taxonomy_term')->load($color_tid);
          $tree[$object->tid]->color_channel = !empty($color_property->field_code_hexadecimal) ? $color_property->field_code_hexadecimal->getValue()[0]['value'] : '';
          $tree[$object->tid]->channel_label = $term_data->field_channel_thematic->getValue()[0]['value'];
        }
      }
    }

    // Populate term's children.
    $tree[$object->tid]->children = [];
    $object_children = &$tree[$object->tid]->children;
    $children = $this->entityTypeManager->getStorage('taxonomy_term')->loadChildren($object->tid);
    if (!$children) {
      return;
    }

    // Recursive population for each child.
    $child_tree_objects = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary, $object->tid);
    foreach ($children as $child) {
      foreach ($child_tree_objects as $child_tree_object) {
        if ($child_tree_object->tid == $child->id()) {
          $this->buildTree($object_children, $child_tree_object, $vocabulary);
        }
      }
    }
  }

  /**
   * Function return domanine term children.
   */
  public function getDomainChild($tid) {
    // Recuperate child  taxonomy term.
    $childrens = $this->entityTypeManager->getStorage('taxonomy_term')->loadChildren($tid);
    // Recuperate language.
    $language = $this->languageManager->getCurrentLanguage()->getId();
    $array_child = [];
    foreach ($childrens as $key => $value) {
      // Recuperate traduction term.
      $term_trad = $this->entityRepository->getTranslationFromContext($value, $language);
      $array_child[$term_trad->getName()]['name'] = $term_trad->getName();
      // Add url objectif.
      if (isset($term_trad->field_objectif_page_link->getValue()[0])) {
        // Convert the URI into URL and push value in array.
        $array_child[$term_trad->getName()]['url'] = Url::fromUri($term_trad->field_objectif_page_link->getValue()[0]['uri'])->toString();
      }
    }
    // Return array data.
    return $array_child;
  }

  /**
   * Function load social link.
   */
  public function loadSocialMedia() {
    $reseau_sociaux_mention = [
      "reseau" => [],
      "mention" => [],
    ];
    $language = $this->languageManager->getCurrentLanguage()->getId();

    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('reseau_sociaux_mentions');
    foreach ($terms as $key => $value) {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($value->tid);

      $term_data = $this->entityRepository->getTranslationFromContext($term, $language);
      $type = $term_data->field_type_link->getValue()[0]['value'];
      $url = Url::fromUri($term_data->field_url->getValue()[0]['uri'])->toString();
      $title = $term_data->name->value;
      if ($type == "social_media") {
        $reseau_sociaux_mention['reseau'][$value->tid] = [];
        $image = File::load($term_data->field_image->getValue()[0]['target_id']);
        $path = $image->url();
        $reseau_sociaux_mention['reseau'][$value->tid]['url'] = $url;
        $reseau_sociaux_mention['reseau'][$value->tid]['title'] = $title;
        $reseau_sociaux_mention['reseau'][$value->tid]['image'] = $path;
      }
      else {
        $reseau_sociaux_mention['mention'][$value->tid] = [];
        $reseau_sociaux_mention['mention'][$value->tid]['url'] = $url;
        $reseau_sociaux_mention['mention'][$value->tid]['title'] = $title;
      }
    }
    return $reseau_sociaux_mention;
  }

}
