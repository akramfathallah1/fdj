<?php

namespace Drupal\c4com_content\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Enable C4COM theme.
 */
class C4comTheme {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Loads the tree of a vocabulary.
   *
   * @var string
   *   The theme name.
   */
  public function setTheme($theme) {
    // Install c4com theme.
    \Drupal::service('theme_installer')->install([$theme]);
    // Set c4com as default theme.
    $this->configFactory->getEditable('system.theme')
      ->set('default', $theme)
      ->save();
  }

}
