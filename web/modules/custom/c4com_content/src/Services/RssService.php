<?php

namespace Drupal\c4com_content\Services;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\file\Entity\File;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Channel Manager Class.
 */
class RssService {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager, LanguageManagerInterface $language_manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->languageManager = $language_manager;

  }

  /**
   * Get  documents XML.
   */
  public  function getDocumentActuality($nids) {
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    $xml_doc = '<documents>';
    foreach ($nodes as $key => $val) {
      if ($val->hasTranslation('fr')) {
        $value = $val->getTranslation('fr');
        $xml_doc .= '<doc><titre_doc><![CDATA['
          . $value->getTitle() . ']]></titre_doc>';
        $xml_doc .= '<url_doc>' . file_create_url($value->field_download_button->entity->field_file->entity->getFileUri()) . '</url_doc>';
        $xml_doc .= '</doc>';
      }
    }
    $xml_doc .= '</documents>';
    return $xml_doc;
  }

  /**
   * Actuality get content (Contenu Field).
   */
  public function getContentActuality($ids) {
    $xml = '<bloc_contenu>';
    // Loop all component in Content field.
    foreach ($ids as $key => $val) {
      $xml .= '<item>';
      // Title block XML.
      $node = $this->entityTypeManager->getStorage('node')->load($val['target_id']);
      if ($node->get('field_titre_block')->getValue()) {
        $xml .= $this->getTitleBlocXml($node->get('field_titre_block')->getValue()[0]['target_id']);
      }
      // Component Value XML.
      if ($node->get('field_add_composant')->getValue()) {
        $xml .= $this->getComposantsXml($node->get('field_add_composant')->getValue());
      }
      $xml .= '</item>';
    }
    $xml .= '</bloc_contenu>';
    return $xml;
  }

  /**
   * Title bloc xml.
   */
  public function getTitleBlocXml($title_id) {
    $xml_tilte = '<titre_bloc><![CDATA[';
    $title = '';
    $para_title = $this->entityTypeManager->getStorage('paragraph')->load($title_id);
    foreach ($para_title->get('field_lignes')->getvalue() as $key => $val) {
      $title .= $val['value'];
    }
    $xml_tilte .= $title . ']]></titre_bloc>';
    return $xml_tilte;
  }

  /**
   * Loop all components.
   */
  public function getComposantsXml($ids) {
    $xml_composants = '<compsants>';
    foreach ($ids as $key => $id) {
      $composant = $this->entityTypeManager->getStorage('paragraph')->load($id['target_id']);
      $xml_composants .= $this->xmlComposant($composant);
    }
    $xml_composants .= '</compsants>';
    return $xml_composants;

  }

  /**
   * Xml component.
   */
  public function xmlComposant($composant) {
    $xml_composant = '<composant>';
    $type = $composant->getType();
    switch ($type) {
      // Text colonne component xml.
      case 'text_conlonnes':
        $xml_composant .= '<type><![CDATA[' . $type . ']]></type><titre><![CDATA['
          . $composant->get('field_title_h3')->getValue()[0]['value'] . ']]></titre><chapo><![CDATA['
          . $composant->get('field_chapo')->getValue()[0]['value'] . ']]></chapo>';
        break;

      // CTA Download component xml.
      case 'paragraph_cta_download':
        $file = File::load($composant->get('field_file')->getValue()[0]['target_id']);
        $path = file_create_url($file->getFileUri());
        $xml_composant .= '<type><![CDATA[' . $type . ']]></type> <url_file>'
          . $path . '</url_file> <description_file><![CDATA['
          . $composant->get('field_file')->getValue()[0]['description'] . ']]></description_file>';

        break;

      // Bloc text media card component xml.
      case 'block_text_media_card':
        $xml_composant .= '<type><![CDATA[' . $type . ']]></type> <titre_block><![CDATA['
          . $composant->get('field_title')->getValue()[0]['value'] . ']]></titre_block><description_bloc><![CDATA['
          . $composant->get('field_texte')->getValue()[0]['value'] . ']]></description_bloc>';
        break;

      // Claim module component xml.
      case 'claim_module':
        $xml_composant .= '<type>' . $type . '</type><teaser>'
          . $composant->get('field_legend_image')->getValue()[0]['value'] . '</teaser><claimtext>'
          . $composant->get('field_text_claim')->getValue()[0]['value'] . '</claimtext>';
        break;
    }
    $xml_composant .= '</composant>';
    return $xml_composant;
  }

}
