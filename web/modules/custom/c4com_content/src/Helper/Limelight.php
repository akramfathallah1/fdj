<?php

namespace Drupal\c4com_content\Helper;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;

/**
 * Class for communication with Limelight external API.
 */
class Limelight {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * Limelight constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger factory.
   */
  public function __construct(LoggerChannelFactoryInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Helper to build the limelight WS URL.
   *
   * @param string $http_verb
   *   The method.
   * @param string $resource_url
   *   The resource url.
   * @param string $access_key
   *   The access key.
   * @param string $secret
   *   The secret.
   * @param array $params
   *   Parameters.
   *
   * @return string
   *   The url.
   */
  protected static function getAuthenticateRequest($http_verb, $resource_url, $access_key, $secret, array $params = NULL) {
    $parsed_url = parse_url($resource_url);
    $str_to_sign = strtolower($http_verb . '|' . $parsed_url['host'] . '|' . $parsed_url['path']) . '|';
    $url = $resource_url . '?';

    if ($params === NULL) {
      $params = [];
    }
    if (!array_key_exists('expires', $params)) {
      $params['expires'] = time() + 3000000;
    }
    $params['access_key'] = $access_key;

    $keys = array_keys($params);
    sort($keys);

    foreach ($keys as $key) {
      $str_to_sign .= $key . '=' . $params[$key] . '&';
      $url .= rawurlencode($key) . '=' . rawurlencode($params[$key]) . '&';
    }

    $str_to_sign = rtrim($str_to_sign, '&');
    $signature = base64_encode(hash_hmac('sha256', $str_to_sign, $secret, TRUE));
    $url .= 'signature=' . rawurlencode($signature);

    return $url;
  }

  /**
   * Helper to get media info from limelight.
   *
   * @param string $media_id
   *   The limelight id.
   *
   * @return array|bool
   *   The media info, FALSE if no video.
   */
  public function getMediaInfo($media_id) {
    // Curl request.
    $params['primary_use'] = 'all';
    // Get Limelight settings.
    $limelight_settings = Settings::get('limelight_api', []);

    if (!empty($limelight_settings)) {
      $url = $limelight_settings['url'];
      $org_id = $limelight_settings['orgId'];
      $access_key = $limelight_settings['accessKey'];
      $secret = $limelight_settings['secret'];

      $use_proxy = $limelight_settings['use_proxy'];
      $proxy_settings = Settings::get('proxy_out', []);

      $request = $url . $org_id . '/media/' . $media_id . '/encodings';
      $signed_request = self::getAuthenticateRequest('GET', $request, $access_key, $secret, $params);

      $curl = curl_init($signed_request);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_TIMEOUT, 10);
      curl_setopt($curl, CURLOPT_HEADER, 0);
      if ($use_proxy) {
        curl_setopt($curl, CURLOPT_PROXY, $proxy_settings['proxy_host'] . ':' . $proxy_settings['proxy_port']);
      }
      $data = curl_exec($curl);
      if (curl_errno($curl)) {
        $error = curl_error($curl);
      }
      curl_close($curl);

      $data = json_decode($data);
    }
    else {
      $error = 'Limelight settings are not set';
    }

    // Error on validation.
    $logger = \Drupal::logger('Limelight');
    if (isset($error)) {
      $logger->error('Limelight error : {error}.', [
        'error' => $error,
      ]);
      return FALSE;
    }
    // Error on data.
    if (empty($data)) {
      $logger->error('Limelight error : Empty data.');
      return FALSE;
    }
    if (!empty($data->errors)) {
      $logger->error('Limelight error : Data containing error(s) {errors}.', [
        'errors' => implode(', ', $data->errors),
      ]);
      return FALSE;
    }
    // Clean limelight result and save it into the BLOB field.
    global $base_url;
    $protocol = parse_url($base_url, PHP_URL_SCHEME) . '://';

    foreach ($data->encodings as $key => &$encodings) {
      // Remove the ThreeGp entry.
      if ($encodings->container_type === 'ThreeGp') {
        unset($data->encodings[$key]);
      }

      // Force https.
      if (!empty($encodings->url)) {
        if (!empty($encodings->file_url)) {
          $encodings_url = $encodings->url;
          $encodings_url_parse = parse_url($encodings_url);
          $encodings_url_scheme = $encodings_url_parse['scheme'];
          if (!in_array($encodings_url_scheme, ['http', 'https'])) {
            $encodings->url = $encodings->file_url;
          }
        }
        $encodings->url = str_replace('http://', $protocol, $encodings->url);
      }
    }
    unset($encodings);
    // Sort encodings by byte size.
    usort($data->encodings, function ($a, $b) {
      return $b->size_in_bytes >= $a->size_in_bytes;
    });

    // Force https.
    foreach ($data->thumbnails as &$thumbnails) {
      if (!empty($thumbnails->url)) {
        $thumbnails->url = str_replace('http://', $protocol, $thumbnails->url);
      }
    }

    // Encode the data.
    return [
      'media_id' => $data->media_id,
      'thumbnail' => !empty($data->thumbnails[1]) ? $data->thumbnails[1] : $data->thumbnails[0],
      'encodings' => $data->encodings,
    ];

  }

}
