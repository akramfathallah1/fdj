<?php

namespace Drupal\c4com_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config Menu Navigation form.
 */
class ConfigWS extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ws_config';
  }

  /**
   * Form build.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('c4com_content.ws_config.settings');

    // Symex WebService configuration.
    $form['url_ws'] = [
      '#type' => 'fieldset',
      '#title' => t('Configuration des web services'),
      '#weight' => 1,
    ];
    $form['url_ws']['url_ws_value'] = [
      '#type' => 'url',
      '#title' => $this->t('URL web service Finance'),
      '#default_value' => $config->get('url_ws_value'),
      '#weight' => 0,
    ];

    // Symex iFrame configuration.
    $form['symex_iframe'] = [
      '#type' => 'fieldset',
      '#title' => t('Symex iframe'),
      '#weight' => 4,
    ];
    $form['symex_iframe']['iframe_url_fr'] = [
      '#type' => 'url',
      '#title' => $this->t('French iframe URL'),
      '#default_value' => $config->get('iframe_url_fr'),
      '#weight' => 3,
    ];
    $form['symex_iframe']['iframe_url_en'] = [
      '#type' => 'url',
      '#title' => $this->t('English iframe URL'),
      '#default_value' => $config->get('iframe_url_en'),
      '#weight' => 2,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 4,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('c4com_content.ws_config.settings')
      ->set('url_ws_value', $form_state->getValue('url_ws_value'))
      ->set('iframe_url_fr', $form_state->getValue('iframe_url_fr'))
      ->set('iframe_url_en', $form_state->getValue('iframe_url_en'))
      ->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['c4com_content.ws_config.settings'];
  }

}
