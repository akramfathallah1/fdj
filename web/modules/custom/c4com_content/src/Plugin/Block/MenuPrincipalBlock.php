<?php

namespace Drupal\c4com_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Menu Principal Block.
 *
 * @Block(
 *   id = "principal_menu",
 *   admin_label = @Translation("Menu principal"),
 * )
 */
class MenuPrincipalBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $items = [];
    $taxo_tree_service = \Drupal::service('c4com_content.taxonomy_term_tree');
    $hero_menu = $taxo_tree_service->load('menu_footer');
    $taxo_tree_service = \Drupal::service('c4com_content.taxonomy_term_tree');
    $channels = $taxo_tree_service->load('channel');
    $config = \Drupal::config('c4com_content.principal_menu.settings');
    if ($config) {
      $items = $config->get();
    }
    return [
      '#theme' => 'principal_menu',
      '#items'  => $items,
      '#channels' => $channels,
      '#hero_menu' => $hero_menu,
      '#logo_path' => theme_get_setting('logo.url'),
      "#is_home" => \Drupal::service('path.matcher')->isFrontPage(),

    ];
  }

}
