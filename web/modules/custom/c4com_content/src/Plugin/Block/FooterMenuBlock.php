<?php

namespace Drupal\c4com_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'FooterMenuBlock' block.
 *
 * @Block(
 *  id = "footer_menu_block",
 *  admin_label = @Translation("Footer menu block"),
 * )
 */
class FooterMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $taxo_tree_service = \Drupal::service('c4com_content.taxonomy_term_tree');
    $tree = $taxo_tree_service->load('menu_footer');
    $taxo_tree_service = \Drupal::service('c4com_content.taxonomy_term_tree');
    $channels = $taxo_tree_service->load('channel');
    $social_media_mentions = $taxo_tree_service->loadSocialMedia();
    return [
      '#theme' => 'footer_menu',
      '#items' => $tree,
      '#channels' => $channels,
      '#social_media_mentions' => $social_media_mentions,
    ];
  }

}
