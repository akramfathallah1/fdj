<?php

namespace Drupal\c4com_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\NodeInterface;

/**
 * Provides a 'AnchorMenuBlock' block.
 *
 * @Block(
 *  id = "anchor_menu_block",
 *  admin_label = @Translation("Anchor menu block"),
 * )
 */
class AnchorMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $anchor = [];
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      if ($node->hasField('field_add_blocks')) {
        $blocks = $node->get('field_add_blocks')->getValue();
        foreach ($blocks as $block) {
          $entityId = $block['target_id'];
          $entitContent = \Drupal::entityTypeManager()->getStorage('node')->load($entityId);
          // Check Paragraph bloc title coming from different content types.
          if (!empty($field = $entitContent->field_bloc_title_parag) || !empty($field = $entitContent->field_bloc_act_for_food_ti)
            || !empty($field = $entitContent->field_titre_block) || !empty($field = $entitContent->field_title_bloc)
            || !empty($field = $entitContent->field_title)) {
            if (!empty($field->getValue())) {
              $titleParaId = $field->getValue()[0]['target_id'];
              if ((\Drupal::entityTypeManager()->getStorage('paragraph')->load($titleParaId)->field_anchor_title) && !empty(\Drupal::entityTypeManager()->getStorage('paragraph')->load($titleParaId)->field_anchor_title->getValue())) {
                $anchorBlockTitle = \Drupal::entityTypeManager()
                  ->getStorage('paragraph')
                  ->load($titleParaId)->field_anchor_title->getValue()[0]['value'];
                $ancreId = str_replace(' ', '-', $anchorBlockTitle);
                $anchor[] = [$ancreId, $anchorBlockTitle];
              }
            }
          }
        }
      }
    }
    return [
      '#theme' => 'anchor_menu',
      '#items' => $anchor,
    ];
  }

}
