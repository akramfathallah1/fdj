<?php

namespace Drupal\c4com_content\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'title_multi_line_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "title_multi_line_field_formatter",
 *   label = @Translation("Title multi line field formatter"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class TitleMultiLineFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $i = 0;
    // Loop all item.
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'title_multiline',
        '#value' => $item->value,
        '#typo'  => ($delta === 0) ? 'manuscrite' : 'capitales',
      ];
      $i++;
    }

    return $elements;
  }

}
