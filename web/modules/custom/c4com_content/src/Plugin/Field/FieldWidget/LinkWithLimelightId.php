<?php

namespace Drupal\c4com_content\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\c4com_content\Helper\Limelight;

/**
 * Plugin implementation of the 'link' widget.
 *
 * @FieldWidget(
 *   id = "link_limelight",
 *   label = @Translation("Link (with limelight ID)"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkWithLimelightId extends LinkWidget implements ContainerFactoryPluginInterface {

  const VIDEO_TYPE_YOUTUBE = 0;

  const VIDEO_TYPE_LIMELIGHT = 1;

  /**
   * Widget alters the existing video url field where enabled.
   */

  /**
   * The API class for Limelight WS.
   *
   * @var \Drupal\c4com_content\Helper\Limelight
   */
  protected $api;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, Limelight $api) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->api = $api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('c4com_content.limelight'));
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#theme'] = 'form_limelight';
    $item = $items[$delta];

    // Id for the ajax selector.
    $id = implode('-', $form['#parents']);

    $options = $item->get('options')->getValue();
    $attributes = $options['attributes'] ?? [];

    $element['options']['attributes']['type'] = [
      '#type' => 'radios',
      '#options' => [
        self::VIDEO_TYPE_LIMELIGHT => '<b>Limelight</b>',
        self::VIDEO_TYPE_YOUTUBE => 'Youtube (Disabled for now)',
      ],
      '#required' => TRUE,
      '#default_value' => $attributes['type'] ?? self::VIDEO_TYPE_YOUTUBE,
      '#weight' => -2,
      '#attributes' => ['id' => 'video-type-' . $id],
    ];

    $element['options']['attributes']['id'] = [
      '#type' => 'textfield',
      '#title' => 'ID Limelight',
      '#weight' => 0,
      '#default_value' => $attributes['id'] ?? '',
      '#states' => [
        'invisible' => [
          ':input[id=video-type-' . $id . ']' => ['value' => self::VIDEO_TYPE_YOUTUBE],
        ],
      ],
    ];

    $element['options']['attributes']['submit'] = [
      '#type' => 'submit',
      '#title' => 'Valider',
      '#value' => 'Valider',
      '#name' => 'submit-' . $id,
      '#weight' => 1,
      '#ajax' => [
        'callback' => [$this, 'validateLimelightId'],
        'progress' => [
          'type' => 'throbber',
        ],
      ],
      '#states' => [
        'invisible' => [
          ':input[id=video-type-' . $id . ']' => ['value' => self::VIDEO_TYPE_YOUTUBE],
        ],
      ],
      // @todo Limit the validation errors when Previewing.
      // '#limit_validation_errors' => [[$items->getName()]],
    ];

    $element['options']['attributes']['blob'] = [
      '#type' => 'textarea',
      '#title' => 'Data Blob (hidden)',
      '#weight' => 3,
      '#default_value' => $attributes['blob'] ?? '',
      '#states' => [
        'invisible' => [
          ':input[id=video-type-' . $id . ']' => ['value' => self::VIDEO_TYPE_YOUTUBE],
        ],
      ],
    ];

    $element['uri']['#states'] = [
      'invisible' => [
        ':input[id=video-type-' . $id . ']' => ['value' => self::VIDEO_TYPE_LIMELIGHT],
      ],
    ];
    $element['uri']['#weight'] = 2;

    $element['#element_validate'] = [[$this, 'elementValidateLimelight']];
    return $element;
  }

  /**
   * Method to validate limelight video.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax response.
   */
  public static function validateLimelightId(array &$form, FormStateInterface $form_state): AjaxResponse {
    $ajax_response = new AjaxResponse();

    // Get the submitted values of the current Video widget.
    // Triggering element is the submit button, we need to find its
    // direct parent.
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    $array_parents = $triggering_element['#array_parents'];
    array_pop($parents);
    array_pop($array_parents);
    $submitted_values = $form_state->getValue($parents);
    $element = NestedArray::getValue($form, $array_parents);

    if (!$form_state->getError($element['id'])) {
      // Limelight was chosen, so we need to display the preview.
      $media_id = $submitted_values['id'];
      // Get the Limelight data, which has already been stored in form state,
      // by the element validator.
      $video_data = $form_state->get('limelight')[$media_id];

      /*
       * Sometime the url attribute is empty in limelight return.
       * we need to filter the encodings with non null url
       * before getting the lighter one.
       */
      $video_urls = [];
      if (!empty($video_data['encodings'])) {
        foreach ($video_data['encodings'] as $encoding) {
          if ($encoding->container_type === 'Mp4' && !empty($encoding->url)) {
            $video_urls[] = $encoding->url;
          }
        }
      }

      // Move at the end of the array to display the lighter video.
      $video_url = end($video_urls);

      $preview_markup = '<p><video width="640" controls><source src="' .
        $video_url . '" type="video/mp4"></video></p>';

      // Display the preview.
      $element['id']['#children'] = [
        '#markup' => $preview_markup,
        '#allowed_tags' => [
          'video',
          'source',
          'p',
        ],
      ];
    }

    // In any case, update the DOM of the 'id' sub-element.
    // This will display the error, or display the preview.
    $ajax_response->addCommand(new ReplaceCommand(
      '.' . str_replace('edit-', 'form-item-', $element['id']['#attributes']['data-drupal-selector']),
      $element['id']
    ));

    return $ajax_response;
  }

  /**
   * Element validate callback.
   *
   * Check the limelight ID, if good, generates blob for the submit.
   *
   * @param array $element
   *   Element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function elementValidateLimelight(array $element, FormStateInterface $form_state): void {
    // Get all submitted values of this element.
    $element_value = $form_state->getValue($element['#parents']);

    // If Limelight video was selected, perform the validation.
    $type = (int) $element_value['options']['attributes']['type'];
    if ($type === self::VIDEO_TYPE_LIMELIGHT) {
      $media_id = $element_value['options']['attributes']['id'];
      if ($media_id) {
        // Call the limelight WS.
        $video_data = $this->api->getMediaInfo($media_id);
        if ($video_data !== FALSE) {
          // Keep the URL value if user has filled it.
          $uri = !empty($element_value['uri']) ? $element_value['uri'] : 'https://youtube.com';
          // Save in the form state the value of the blob.
          $encoded_video_data = json_encode($video_data);
          $blob = !empty($encoded_video_data) ? $encoded_video_data : '';

          $new_element_value = [
            'uri' => $uri,
            'title' => '',
            'options' => [
              'attributes' => [
                'type' => $type,
                'id' => $media_id,
                'blob' => $blob,
              ],
            ],
          ];

          $form_state->setValueForElement($element, $new_element_value);

          // Save Limelight data of this ID in form state,
          // in order to be reused in AJAX callback.
          $limelight_info = $form_state->get('limelight');
          $limelight_info[$media_id] = $video_data;
          $form_state->set('limelight', $limelight_info);
        }
        else {
          $form_state->setError($element['options']['attributes']['id'], $this->t('Could not retrieve the video from Limelight.'));
        }
      }
      else {
        $form_state->setError($element['options']['attributes']['id'], $this->t('Please fill in the Limelight ID.'));
      }
    }
  }

}
