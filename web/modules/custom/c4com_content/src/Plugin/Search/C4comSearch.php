<?php

namespace Drupal\c4com_content\Plugin\Search;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Database\Query\Condition;
use Drupal\Search\SearchQuery;
use Drupal\node\Plugin\Search\NodeSearch;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Unicode;
/**
 * Handles searching for node entities using the Search module index.
 *
 * @SearchPlugin(
 *   id = "c4com_search",
 *   title = @Translation("C4COM")
 * )
 */
class C4comSearch extends NodeSearch {

  /**
   * Queries to find search results, and sets status messages.
   *
   * This method can assume that $this->isSearchExecutable() has already been
   * checked and returned TRUE.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   Results from search query execute() method, or NULL if the search
   *   failed.
   */
  protected function findResults() {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $allowed_bundles = ['landing_page', 'actu_mag', 'channel_page'];
    $keys = $this->keywords;
    // Build matching conditions.
    $query = $this->databaseReplica
      ->select('search_index', 'i')
      ->extend('Drupal\search\SearchQuery')
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    $query->join('node_field_data', 'n', 'n.nid = i.sid AND n.langcode = i.langcode');
    $query->condition('n.status', 1)
      ->condition('n.type', $allowed_bundles, 'IN')
      ->condition('n.langcode', $language)
      ->addTag('node_access')
      ->searchExpression($keys, $this->getPluginId());

    // Handle advanced search filters in the f query string.
    // \Drupal::request()->query->get('f') is an array that looks like this in
    // the URL: ?f[]=type:page&f[]=term:27&f[]=term:13&f[]=langcode:en
    // So $parameters['f'] looks like:
    // array('type:page', 'term:27', 'term:13', 'langcode:en');
    // We need to parse this out into query conditions, some of which go into
    // the keywords string, and some of which are separate conditions.
    $parameters = $this->getParameters();
    if (!empty($parameters['f']) && is_array($parameters['f'])) {
      $filters = [];
      // Match any query value that is an expected option and a value
      // separated by ':' like 'term:27'.
      $pattern = '/^(' . implode('|', array_keys($this->advanced)) . '):([^ ]*)/i';
      foreach ($parameters['f'] as $item) {
        if (preg_match($pattern, $item, $m)) {
          // Use the matched value as the array key to eliminate duplicates.
          $filters[$m[1]][$m[2]] = $m[2];
        }
      }

      // Now turn these into query conditions. This assumes that everything in
      // $filters is a known type of advanced search.
      foreach ($filters as $option => $matched) {
        $info = $this->advanced[$option];
        // Insert additional conditions. By default, all use the OR operator.
        $operator = empty($info['operator']) ? 'OR' : $info['operator'];
        $where = new Condition($operator);
        foreach ($matched as $value) {
          $where->condition($info['column'], $value);
        }
        $query->condition($where);
        if (!empty($info['join'])) {
          $query->join($info['join']['table'], $info['join']['alias'], $info['join']['condition']);
        }
      }
    }

    // Add the ranking expressions.
    $this->addNodeRankings($query);

    // Run the query.
    $find = $query
      // Add the language code of the indexed item to the result of the query,
      // since the node will be rendered using the respective language.
      ->fields('i', ['langcode'])
      // And since SearchQuery makes these into GROUP BY queries, if we add
      // a field, for PostgreSQL we also need to make it an aggregate or a
      // GROUP BY. In this case, we want GROUP BY.
      ->groupBy('i.langcode')
      ->limit(10)
      ->execute();

    // Check query status and set messages if needed.
    $status = $query->getStatus();

    if ($status & SearchQuery::EXPRESSIONS_IGNORED) {
      $this->messenger->addWarning($this->t('Your search used too many AND/OR expressions. Only the first @count terms were included in this search.', ['@count' => $this->searchSettings->get('and_or_limit')]));
    }

    if ($status & SearchQuery::LOWER_CASE_OR) {
      $this->messenger->addWarning($this->t('Search for either of the two terms with uppercase <strong>OR</strong>. For example, <strong>cats OR dogs</strong>.'));
    }

    if ($status & SearchQuery::NO_POSITIVE_KEYWORDS) {
      $this->messenger->addWarning($this->formatPlural($this->searchSettings->get('index.minimum_word_size'), 'You must include at least one keyword to match in the content, and punctuation is ignored.', 'You must include at least one keyword to match in the content. Keywords must be at least @count characters, and punctuation is ignored.'));
    }

    return $find;
  }

  /**
   * Prepares search results for rendering.
   *
   * @param \Drupal\Core\Database\StatementInterface $found
   *   Results found from a successful search query execute() method.
   *
   * @return array
   *   Array of search result item render arrays (empty array if no results).
   */
  protected function prepareResults(StatementInterface $found) {
    $results = [];

    $node_storage = $this->entityManager->getStorage('node');
    $node_render = $this->entityManager->getViewBuilder('node');
    $keys = $this->keywords;

    foreach ($found as $item) {
      // Render the node.
      /** @var \Drupal\node\NodeInterface $node */
      $node = $node_storage->load($item->sid)->getTranslation($item->langcode);
      $build = $node_render->view($node, 'search', $item->langcode);

      /** @var \Drupal\node\NodeTypeInterface $type*/
      $type = $this->entityManager->getStorage('node_type')->load($node->bundle());

      unset($build['#theme']);
      $build['#pre_render'][] = [$this, 'removeSubmittedInfo'];

      // Fetch comments for snippet.
      $rendered = $this->renderer->renderPlain($build);
      $this->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));
      $rendered .= ' ' . $this->moduleHandler->invoke('comment', 'node_update_index', [$node]);

      $extra = $this->moduleHandler->invokeAll('node_search_result', [$node]);

      $username = [
        '#theme' => 'username',
        '#account' => $node->getOwner(),
      ];
      $result = [
        'link' => $node->toUrl('canonical', ['absolute' => TRUE])->toString(),
        'type' => $type->label(),
        'title' => $node->label(),
        'node' => $node,
        'extra' => $extra,
        'score' => $item->calculated_score,
        'snippet' => $this->searchExcerpt($keys, $rendered, $item->langcode),
        'langcode' => $node->language()->getId(),
        'created' => date('d/m/Y',$node->get('created')->getString())

      ];
      $this->addCacheableDependency($node);

      // We have to separately add the node owner's cache tags because search
      // module doesn't use the rendering system, it does its own rendering
      // without taking cacheability metadata into account. So we have to do it
      // explicitly here.
      $this->addCacheableDependency($node->getOwner());

      if ($type->displaySubmitted()) {
        $result += [
          'user' => $this->renderer->renderPlain($username),
          'date' => $node->getChangedTime(),
        ];
      }

      $results[] = $result;

    }

    return $results;
  }

  /**
   * Returns snippets from a piece of text, with search keywords highlighted.
   *
   * Used for formatting search results. All HTML tags will be stripped from
   * $text.
   *
   * @param string $keys
   *   A string containing a search query.
   * @param string $text
   *   The text to extract fragments from.
   * @param string|null $langcode
   *   Language code for the language of $text, if known.
   *
   * @return array
   *   A render array containing HTML for the excerpt.
   */
  private function searchExcerpt($keys, $text, $langcode = NULL) {
    // We highlight around non-indexable or CJK characters.
    $boundary_character = '[' . Unicode::PREG_CLASS_WORD_BOUNDARY . PREG_CLASS_CJK . ']';
    $preceded_by_boundary = '(?<=' . $boundary_character . ')';
    $followed_by_boundary = '(?=' . $boundary_character . ')';
    // Extract positive keywords and phrases.
    preg_match_all('/ ("([^"]+)"|(?!OR)([^" ]+))/', ' ' . $keys, $matches);
    $keys = array_merge($matches[2], $matches[3]);
    // Prepare text by stripping HTML tags and decoding HTML entities.
    $text = strip_tags(str_replace(['<', '>'], [' <', '> '], $text));
    $text = Html::decodeEntities($text);
    $text_length = strlen($text);

    // Make a list of unique keywords that are actually found in the text,
    // which could be items in $keys or replacements that are equivalent through
    // search_simplify().
    $temp_keys = [];
    foreach ($keys as $key) {
      $key = _search_find_match_with_simplify($key, $text, $boundary_character, $langcode);
      if (isset($key)) {
        // Quote slashes so they can be used in regular expressions.
        $temp_keys[] = preg_quote($key, '/');
      }
    }
    // Several keywords could have simplified down to the same thing, so pick
    // out the unique ones.
    $keys = array_unique($temp_keys);

    // Extract fragments of about 60 characters around keywords, bounded by word
    // boundary characters. Try to reach 256 characters, using second occurrences
    // if necessary.
    $ranges = [];
    $length = 0;
    $look_start = [];
    $remaining_keys = $keys;

    while ($length < 256 && !empty($remaining_keys)) {
      $found_keys = [];
      foreach ($remaining_keys as $key) {
        if ($length >= 256) {
          break;
        }

        // Remember where we last found $key, in case we are coming through a
        // second time.
        if (!isset($look_start[$key])) {
          $look_start[$key] = 0;
        }

        // See if we can find $key after where we found it the last time. Since
        // we are requiring a match on a word boundary, make sure $text starts
        // and ends with a space.
        $matches = [];
        if (preg_match('/' . $preceded_by_boundary . $key . $followed_by_boundary . '/iu', ' ' . $text . ' ', $matches, PREG_OFFSET_CAPTURE, $look_start[$key])) {
          $found_position = $matches[0][1];
          $look_start[$key] = $found_position + 1;
          // Keep track of which keys we found this time, in case we need to
          // pass through again to find more text.
          $found_keys[] = $key;

          // Locate a space before and after this match, leaving about 60
          // characters of context on each end.
          $before = strpos(' ' . $text, ' ', max(0, $found_position - 61));
          if ($before !== FALSE && $before <= $found_position) {
            if ($text_length > $found_position + 60) {
              $after = strrpos(substr($text, 0, $found_position + 60), ' ', $found_position);
            }
            else {
              $after = $text_length;
            }
            if ($after !== FALSE && $after > $found_position) {
              // Account for the spaces we added.
              $before = max($before - 1, 0);
              if ($before < $after) {
              $ranges[$before] = $after;
              $length += $after - $before;
            }
            }
          }
        }
      }
      // Next time through this loop, only look for keys we found this time,
      // if any.
      $remaining_keys = $found_keys;
    }

    if (empty($ranges)) {
      // We didn't find any keyword matches, so just return the first part of the
      // text. We also need to re-encode any HTML special characters that we
      // entity-decoded above.
      return [
        '#plain_text' => Unicode::truncate($text, 256, TRUE, TRUE),
      ];
    }

    // Sort the text ranges by starting position.
    ksort($ranges);

    // Collapse overlapping text ranges into one. The sorting makes it O(n).
    $new_ranges = [];
    $max_end = 0;
    foreach ($ranges as $this_from => $this_to) {
      $max_end = max($max_end, $this_to);
      if (!isset($working_from)) {
        // This is the first time through this loop: initialize.
        $working_from = $this_from;
        $working_to = $this_to;
        continue;
      }
      if ($this_from <= $working_to) {
        // The ranges overlap: combine them.
        $working_to = max($working_to, $this_to);
      }
      else {
        // The ranges do not overlap: save the working range and start a new one.
        $new_ranges[$working_from] = $working_to;
        $working_from = $this_from;
        $working_to = $this_to;
      }
    }
    // Save the remaining working range.
    $new_ranges[$working_from] = $working_to;

    // Fetch text within the combined ranges we found.
    $out = [];
    foreach ($new_ranges as $from => $to) {
      $out[] = substr($text, $from, $to - $from);
    }

    // Combine the text chunks with "…" separators. The "…" needs to be
    // translated. Let translators have the … separator text as one chunk.
    //$ellipses = explode('@excerpt', t('… @excerpt … @excerpt …'));
    //$text = (isset($new_ranges[0]) ? '' : $ellipses[0]) . implode($ellipses[1], $out) . (($max_end < strlen($text) - 1) ? $ellipses[2] : '');
    $text = Html::escape($text);

    // Highlight keywords. Must be done at once to prevent conflicts ('strong'
    // and '<strong>').
    $text = trim(preg_replace('/' . $preceded_by_boundary . '(?:' . implode('|', $keys) . ')' . $followed_by_boundary . '/iu', '<span class="found">\0</span>', ' ' . $text . ' '));
    return [
      '#markup' => $text,
      '#allowed_tags' => ['span'],
    ];
  }

}
