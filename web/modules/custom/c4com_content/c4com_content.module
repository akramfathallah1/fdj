<?php

/**
 * @file
 * Contains c4com_content.module..
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Database\Query\AlterableInterface;


/**
 * Implements hook_help().
 */
function rc4com_content_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.c4com_content':
      $output = '';
      $output .= '<h3>' . t("About") . '</h3>';
      $output .= '<p>' . t("All content related functions and services") . '</p>';
      return $output;

    default:
      return '';
  }
}

/**
 * Implements hook_theme().
 */
function c4com_content_theme($existing, $type, $theme, $path) {
  return [
    'principal_menu' => [
      'variables' => [
        'items' => NULL,
        'hero_menu' => NULL,
        'channels' => NULL,
        'logo_path' => NULL,
        'languages' => NULL,
        'is_home' => NULL,
      ],
      'template' => 'menu--principale',
    ],
    'footer_menu' => [
      'variables' => [
        'items' => NULL,
        'channels' => NULL,
        'social_media_mentions' => NULL,
      ],
      'template' => 'menu--footer',
    ],
    'anchor_menu' => [
      'variables' => [
        'items' => NULL,
      ],
      'template' => 'menu--anchor',
    ],
    'title_multiline' => [
      'variables' => [
        'value' => NULL,
        'typo' => NULL,
      ],
      'template' => 'title--multiline',
    ],
    'breadcrumb' => [
      'variables' => [
        'items' => NULL,
      ],
      'template' => 'breadcrumb',
    ],
    'form_limelight' => [
      'template' => 'form--limelight',
      'render element' => 'element',
    ],
  ];
}

/**
 * Implements hook_ckeditor_css_alter().
 */
function c4com_content_ckeditor_css_alter(&$css, $editor) {
  $css[] = drupal_get_path('theme', 'c4com') . '/css/ckeditor_custom_styles.css';
}

/**
 * Implements hook_locale_translation_projects_alter().
 */
function c4com_content_locale_translation_projects_alter(&$projects) {
  $projects['c4com_content'] = [
    'name' => 'c4com_content',
    'info' => [
      'name' => 'C4COM Content',
      'interface translation server pattern' => 'modules/custom/%project/translations/%project.%language.po',
      'project' => 'c4com_content',
    ],
    'project_type' => 'module',
    'project_status' => TRUE,
  ];
}

/**
 * Implements hook_preprocess_node().
 */
function c4com_content_preprocess_node(&$variables) {

  $node = $variables['node'];
  $type = $node->getType();
  switch ($type) {
    case 'bloc_remontee':
      // Get embed 'Last 3 elements' view if 'bloc_remontee' is added.
      $type_id = $node->get('field_tag_type')->getValue()[0]['target_id'];
      $view_embed = views_embed_view('last_three', 'embed_1', $type_id);
      $variables['list_last'] = render($view_embed);
      break;

    case $type === 'block_domaine':
      $taxo_tree_service = \Drupal::service('c4com_content.taxonomy_term_tree');
      $domaine_id = $node->get('field_domaine')->getValue()[0]['target_id'];
      $objectis = $taxo_tree_service->getDomainChild($domaine_id);
      $variables['list_objectif'] = $objectis;
      break;

    case 'block_full_list':
      $view = $node->get('field_select_view_list')->getValue()[0]['value'];
      $view_embed = views_embed_view($view, 'embed_1');
      $variables['full_list'] = render($view_embed);
      break;

    case 'module_banniere':
      // Verify type bloc (content or financial)
      $block_type = $node->get('field_choose_block_type')->getValue()[0]['value'];
      if ($block_type === 'block_finance') {
        $financial_service = \Drupal::service('c4com_content.client.http');
        $config = \Drupal::config('c4com_content.ws_config.settings');
        if (!empty($config->get()['url_ws_value'])) {
          $variables['fiancial_data'] = $financial_service->get($config->get()['url_ws_value']);
        }
      }
      break;

    case 'actus_three_bloc':
      $arg = '';
      $categories_values = $node->get('field_categories_articles')->getValue();
      foreach ($categories_values as $key => $value) {
        $arg .= $value['target_id'] . '+';
      }
      if (strlen($arg)) {
        $arg = substr_replace($arg, "", -1);
        $view_embed = views_embed_view('last_three_articles', 'embed_1', $arg);
      }
      else {
        $view_embed = views_embed_view('last_three_articles', 'embed_1');
      }
      $variables['three_actus_list'] = render($view_embed);
      break;

    case 'channel_page':
      // Verify type bloc (content or financial)
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $channel_service = \Drupal::service('c4com_content.channelmanager');
      $mag_banner = $node->field_mag_article->getValue()[0]['target_id'];
      $thematic = $node->field_thematic->getValue()[0]['target_id'];
      $list_magazine = $channel_service->getArticleByChannel($thematic, $mag_banner);
      $variables['list_magazine'] = $list_magazine;
      $variables['lang'] = $language;

      break;

    case 'bloc_stock_iframe':
      $config = \Drupal::config('c4com_content.ws_config.settings');
      $lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $variables['iframe_url'] = NULL;
      // Get iframe url depending on site language.
      if (!empty($config->get()['iframe_url_' . $lang_code])) {
        $variables['iframe_url'] = $config->get()['iframe_url_' . $lang_code];
      }
      break;

    case 'carrefour_ecosystem_bloc':
      $filter = [t('Integrated countries')->__toString(), t('Partner countries')->__toString()];
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $query = \Drupal::entityQuery('taxonomy_term');
      $query->condition('vid', "region");
      $query->sort('weight');
      $tids = $query->execute();
      $terms = Term::loadMultiple($tids);
      foreach ($terms as $term) {
        if ($term->hasTranslation($language)) {
          $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $language);
          array_push($filter, $translated_term->getName());
        }
      }
      $variables['filter'] = json_encode($filter);
      $variables['lang'] = $language;
      break;

    case 'bloc_fiches_metiers':
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $variables['lang'] = $language;
      break;
  }
}

/**
 * Implements hook__preprocess_views_view().
 */
function c4com_content_preprocess_views_view__view_financial_publications__embed_1(&$variables) {
  $view = $variables['view'];
  $term = Term::load($view->exposed_raw_input['field_publication_type_target_id']);
  $variables['header'] = $term->getName() . ' ' . $view->exposed_raw_input['field_year_value'];
}
