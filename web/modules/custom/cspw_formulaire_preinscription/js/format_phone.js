"use strict";

var edit_portable = document.getElementById('edit-portable');
if (edit_portable){
    edit_portable.addEventListener('input', function (e) {
        e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{2})/g, '$1 ').trim();
    });
}
var edit_portable_responsable = document.getElementById('edit-portable-responsable');
if (edit_portable_responsable){
    edit_portable_responsable.addEventListener('input', function (e) {
        e.target.value = e.target.value.replace(/[^\dA-Z]/g, '').replace(/(.{2})/g, '$1 ').trim();
    });
}

