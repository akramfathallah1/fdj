(function ($, Drupal) {
  Drupal.behaviors.cspw_formulaire_preinscription = {
    attach: function (context, settings) {
      $(context).find('.form-control-require').once('formAsterix').each(function () {
        $(this).parent('.input-group, .select-wrapper').siblings('label.control-label').addClass('form-required-asterix');
        $(this).siblings('label.control-label').addClass('form-required-asterix');
      });

      //for documentation visit: https://bootstrap-datepicker.readthedocs.io/en/stable/

      $(context).find('input.bs-date-picker-anniv').once('formDatePicker').each(function () {
          $(this).datepicker({
            maxViewMode: 2,
            endDate: "-12y",
            format: "yyyy-mm-dd",
            todayBtn: true,
            language: "fr",
            daysOfWeekDisabled: "1",
            autoclose: true,
            todayHighlight: true,
            showOnFocus: true,
            beforeShowDay: function(date){
            },
            beforeShowMonth: function(date){
            },
            beforeShowYear: function(date){
            }
          });
      });
    }
  };
})(jQuery, Drupal);
