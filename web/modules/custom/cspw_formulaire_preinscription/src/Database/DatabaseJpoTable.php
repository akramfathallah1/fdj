<?php


namespace Drupal\cspw_formulaire_preinscription\Database;

/*
 ** class DatabaseJpoTable pour faire des requete sur  la table  cspw_jpo_event.
 */
class DatabaseJpoTable{

    protected $database;
    // Class constructor.
    public function __construct() {
        $this->database = \Drupal::database();
        $this->lastId = 0;
    }

    // Add new line.
    public function InserNewLineJpo($fields){
        $this->database->insert('cspw_jpo_event')
        ->fields($fields)
        ->execute();
    }
    // Get element by id
    public function GetElementById($id){
      $query = $this->database->select('cspw_jpo_event','jpo');
      $query->fields('jpo');
      $query->condition('id' ,$id);
      $data = $query->execute();
      $data = $data-> fetchAll();
      return $data;
    }
    // Update line jpo
    public function updateLineJpo ($fields, $id){
      $query = $this->database->update('cspw_jpo_event')
        ->condition('id', $id)
        ->fields($fields)
        ->execute();
    }
    // Delete line jpo
    public function deleteLineJpo ($id){
        $query = $this->database->delete('cspw_jpo_event')
        ->condition('id', $id)
        ->execute();
  }

}
