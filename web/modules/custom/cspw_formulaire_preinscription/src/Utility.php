<?php

namespace Drupal\cspw_formulaire_preinscription;

use Drupal\cspw_formulaire_preinscription\Database\DatabaseFormulaire;

class Utility {

protected $database;
    /*
     * __construct()
     */

    public function __construct() {
    $this->database = new DatabaseFormulaire();
    }

    /*
     * Verifie et renvoi true ou false si la template existe
     * celon le code ecole
     */

    public function checkTemplateFromCodeEcole($enseigne, $base_hook) {
        $moduleHandler = \Drupal::service('module_handler');
        if ($moduleHandler->moduleExists($enseigne)) {

           return file_exists(drupal_get_path('module', $enseigne) . '/templates/' . $base_hook . '-' . $enseigne . '.html.twig');
        }
        else {
            return false;
        }
    }

    /*
    * Verifie si un candidat existe déjà
    * Possibilité de params =
     * $params->mail
     * $params->filliere
    */

    public function checkUserExist($params) {

       $data =  $this->database->SelectCandidat($params);
       if($data != null){
           $exist = true;
       }
       else{
           $exist = false;
       }
        return $exist;
    }
}
