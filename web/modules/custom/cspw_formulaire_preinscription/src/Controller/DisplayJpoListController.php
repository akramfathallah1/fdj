<?php
namespace Drupal\cspw_formulaire_preinscription\Controller;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\cspw_formulaire_preinscription\Database\DatabaseJpoTable;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Link;
/**
 * Class DisplayTableController.
 *
 * @package Drupal\mydata\Controller
 */
class DisplayJpoListController extends ControllerBase
{

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;
  protected $id;
  protected $operation;
  protected $database;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->id = \Drupal::routeMatch()->getParameter('id');
    $this->operation = \Drupal::routeMatch()->getParameter('operation');
    $this->database =  new DatabaseJpoTable();
  }

  /**
   * Static funtion (DI)
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static($entity_manager);
  }
  public function getContent()
  {
    // First we'll tell the user what's going on. This content can be found
    // in the twig template file: templates/description.html.twig.
    // @todo: Set up links to create nodes and point to devel module.
    $build = [
      'description' => [
        '#theme' => 'mydata_description',
        '#description' => 'foo',
        '#attributes' => [],
      ],
    ];
    return $build;
  }

  /**
   * Display.
   *
   * @return string
   *   Return Hello string.
   */
  public function display(){
    $userRole = \Drupal::currentUser()->getRoles();
    $listEcole = [];
    if(in_array('administrator',$userRole)) {
      // if administrator get all taxonomy "ecole" terms and create radio field and set default value.
      $vid = "les_ecoles";
      $i = 1;
      $ecoles = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($ecoles as $term) {
        $url = Url::fromRoute('cspw_jpo_event_form.jpo_list', array('id' => $term->tid));
        $project_link = Link::fromTextAndUrl($term->name, $url);
        $project_link = $project_link->toRenderable();
        if($this->id == ""){
          if($i == 1){
            $project_link['#attributes'] = array('class' => array('current-item-list'));
            $this->id = $term->tid;
          }
        }elseif($this->id == $term->tid){
          $project_link['#attributes'] = array('class' => array('current-item-list'));
        }
        $listEcole[$term->tid] = render($project_link);
        $i++;
      }
    }else {
      // if is not admin get user "ecole" and create field radio.
      $fullUserInfo = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());
      $userEcoleId = $fullUserInfo->field_user_code_ecole->getValue()[0]['target_id'];
      $ecoleName = $this->entityTypeManager->getStorage('taxonomy_term')->load($userEcoleId)->getName();
      $form['ecole'] = [
        '#title' => 'ffff',
        '#type' => 'radios',
        '#options' => [$userEcoleId => $ecoleName],
        '#default_value' => $userEcoleId,
        '#required' => True,
      ];
    }
    //create table header
    $header_table = array(
      'id' => t('#'),
      'envent_name' => t('Nom de l\'événement'),
      'event_salon' => t('Salon / forum / JPO'),
      'event_ecole' => t('Ecole'),
    );

//select records from table
    $query = \Drupal::database()->select('cspw_jpo_event', 'jpo');
    $query->fields('jpo');
    $query->condition( 'event_ecole', $this->id);
    $results = $query->execute()->fetchAll();
    $rows = array();
    foreach ($results as $data) {
        $url_edit = Url::fromRoute('cspw_jpo_event_form.jpo_add_edit', array('operation' => 'edit','id' => $data->id));
        $link_edit = Link::fromTextAndUrl($this->t('EDIT'), $url_edit);

        $link_edit = $link_edit->toRenderable();
      $url_delete = Url::fromRoute('cspw_jpo_event_form.jpo_delete', array('id' => $data->id));
      $link_delete = Link::fromTextAndUrl($this->t('DELETE'), $url_delete);
      $link_delete = $link_delete->toRenderable();

      //print the data from table
      $rows[] = array(
        'id' => $data->id,
        'Nom de l\'événement' => $data->event_name,
        'Salon / forum / JPO' => $data->event_salon,
        'Ecole' => $data->event_ecole,
        'opreation1' =>  render($link_edit),
        'operation2'=> render($link_delete),


        //\Drupal::l('Delete', $delete),
        //\Drupal::l('Edit', $edit),
      );
    }

    $form['table1'] = [
      '#type' => 'table',
      '#header' => $listEcole,
    ];
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header_table,
      '#rows' => $rows,
      '#empty' => t("Pas d'événement"),
    ];
    return $form;

  }
}
