<?php

namespace Drupal\cspw_formulaire_preinscription;
use Drupal\cspw_api_client\ApiClientService;

/**
 * Class ExempleService.
 */
class YpareoService implements YpareoServiceInterface {

  /**
   * Drupal\cspw_api_client\ApiClientService definition.
   *
   * @var \Drupal\cspw_api_client\ApiClientService
   */
  protected $cspwApiClientConnector;
  /**
   * Constructs a new InbentaService object.
   */
  public function __construct(ApiClientService $cspw_api_client_connector) {
    $this->cspwApiClientConnector = $cspw_api_client_connector;
  }

  /**
   * Retourne le type de l'événement en GET
   */

  public function getFormationsResult() {

      global $config;
      $result = [];
      $endPoint_formation = ApiClientService::$cspwWsConf['FORMATIONS']['URL'];
      $endPoint = ApiClientService::$cspwWsConf['YPAREO_ENDPOINT']['URL'].$endPoint_formation;
      $param_apis= [
        'headers' => [
            "X-Auth-Token" => $config['webservice']['ypareo']['X_Auth_Token'],
        ],
      ];

    $wsResult = $this->cspwApiClientConnector->request('FORMATIONS', $endPoint,$param_apis);
    // verifier est ce que le ws retourn le code status 200 ou pas
    if($wsResult->getStatusCode() == 200){
        $body = $wsResult->getBody();
        $content =  $body->getContents();
        $result = json_decode($content, true);
    }
      return $result;
  }

    /**
     * Retourne liste formation
     */

    public function getSitesFormationsResult() {
        global $config;
        $result = [];
        $endPoint_sites_formation = ApiClientService::$cspwWsConf['SITES']['URL'];

        $endPoint = ApiClientService::$cspwWsConf['YPAREO_ENDPOINT']['URL'].$endPoint_sites_formation;
        $param_apis= [
            'headers' => [
                "X-Auth-Token" => $config['webservice']['ypareo']['X_Auth_Token'],
            ],
        ];

        $wsResult = $this->cspwApiClientConnector->request('SITES', $endPoint,$param_apis);
        // verifier est ce que le ws retourn le code status 200 ou pas
        if($wsResult->getStatusCode() == 200){
            $body = $wsResult->getBody();
            $content =  $body->getContents();
            $result = json_decode($content, true);
        }
        return $result;
    }

}
