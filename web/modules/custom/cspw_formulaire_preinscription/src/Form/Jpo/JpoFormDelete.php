<?php

/**
 * @file
 * Contains \Drupal\cspw_formulaire_preinscription\Form\Jpo\JpoForm.
 */
namespace Drupal\cspw_formulaire_preinscription\Form\Jpo;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\cspw_formulaire_preinscription\Database\DatabaseJpoTable;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Url;
use Drupal\Core\Form\ConfirmFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Path\PathValidatorInterface;

/**
 * JpoForm class build.
 */
class JpoFormDelete extends ConfirmFormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  protected $database;
  // id
  protected $id;
  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager , PathValidatorInterface $path_validator ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pathValidator = $path_validator;
    $this->id = \Drupal::routeMatch()->getParameter('id');
    $this->database =  new DatabaseJpoTable();
  }

  /**
   * Static funtion (DI)
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    $pathvalidator = $container->get('path.validator');
    return new static($entity_manager , $pathvalidator);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_jpo_form';
  }
  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Etes vous sur de supprimer l\'événement.');
  }
  /**
   *  Cancel URL
   */
  public function getCancelUrl() {
    // Get the request data.
    $request = Request::createFromGlobals();
    // Get origin url.
    $uri = parse_url($request->headers->get('referer'), PHP_URL_PATH);
    // Get the URL object
    $urlObject = $this->pathValidator->getUrlIfValid($uri);
    $routeName = $urlObject->getRouteName();
    return new Url($routeName , ['operation' => 'edit' , 'id' => $this->id]);
  }
  /**
   *  Description
   */
  public function getDescription() {
    return $this->t('Etes vous sur de supprimer l\'événement.');
  }
  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }
  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return t('Cancel');
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $cid = NULL) {
    $line = $this->database->GetElementById($this->id);
    $userRole = \Drupal::currentUser()->getRoles();
    // pas authorisation si il n'y a pas de ligne dans la base.
    if(!$line){
      throw new AccessDeniedHttpException();
    }
    // pour un user autre que admin bloque s'il veut mofifier une line qui n'appatien pas à son ecole
    if(!in_array('administrator',$userRole)) {
      $userInfo = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());
      $userEcoleId = $userInfo->field_user_code_ecole->getValue()[0]['target_id'];
      if( $line[0]->event_ecole != $userEcoleId ){
        throw new AccessDeniedHttpException();
      }
    }
    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = \Drupal::messenger();
    $this->database->deleteLineJpo( $this->id);
    $messenger->addStatus("Suppression avec succes!");
    $form_state->setRedirect("cspw_jpo_event_form.jpo_add_edit" , ['operation' => 'add', 'id'=> '']);
  }

}
