<?php

/**
 * @file
 * Contains \Drupal\cspw_formulaire_preinscription\Form\Jpo\JpoFormAddEdit.
 */
namespace Drupal\cspw_formulaire_preinscription\Form\Jpo;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\cspw_formulaire_preinscription\Database\DatabaseJpoTable;
use Drupal\Component\Utility\Xss;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\Url;

/**
 * JpoForm class build.
 */
class JpoFormAddEdit extends FormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;
  protected $id;
  protected $operation;
  protected $database;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->id = \Drupal::routeMatch()->getParameter('id');
    $this->operation = \Drupal::routeMatch()->getParameter('operation');
    $this->database =  new DatabaseJpoTable();
  }

  /**
   * Static funtion (DI)
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static($entity_manager);
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'jpo_form_add';
  }

  /**
   * Form build.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Attacher fichier de style.
    $form['#attached']['library'][] = 'cspw_formulaire_preinscription/cspw_formulaire_presinascription.backoffice';
    $line = [];
    // Get current user role.
    $userRole = \Drupal::currentUser()->getRoles();
    if($this->operation == "edit"){
      if($this->id == ""){
        throw new AccessDeniedHttpException();
      }else{
        $line = $this->database->GetElementById($this->id);
        // If no line access denied
        if(!$line){
          throw new AccessDeniedHttpException();
        }
      }
    }
    // Verify if current user is aministrator.
    if(in_array('administrator',$userRole)) {
      // if administrator get all taxonomy "ecole" terms and create radio field and set default value.
      $vid = "les_ecoles";
      $ecoles = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vid);
      foreach ($ecoles as $term) {
        $term_data[$term->tid] = $term->name;
      }
      $form['ecole'] = [
        '#type' => 'radios',
        '#options' => $term_data,
        '#default_value' => ($line) ? $line[0]->event_ecole:array_keys($term_data)[0],
        '#required' => True,
      ];
    }else {
      // if is not admin get user "ecole" and create field radio.
      $fullUserInfo = $this->entityTypeManager->getStorage('user')->load(\Drupal::currentUser()->id());
      $userEcoleId = $fullUserInfo->field_user_code_ecole->getValue()[0]['target_id'];
      if( $this->operation == "edit" && $userEcoleId != $line[0]->event_ecole){
        throw new AccessDeniedHttpException();
      }
      $ecoleName = $this->entityTypeManager->getStorage('taxonomy_term')->load($userEcoleId)->getName();
      $form['ecole'] = [
        '#type' => 'radios',
        '#options' => [$userEcoleId => $ecoleName],
        '#default_value' => $userEcoleId,
        '#required' => True,
      ];
    }
    $form['event_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Nom de l'événement"),
      '#maxlength' => 150,
      '#default_value' => ($line) ? $line[0]->event_name:'',
      '#required' => True,
    ];
    $form['event_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Texte d'introduction" ),
      '#rows' => 5,
      '#default_value' => ($line) ? $line[0]->event_description:'',
      '#required' => True,
    ];
    $form['event_salon'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Salon / forum / JPO" ),
      '#default_value' => ($line) ? $line[0]->event_salon:'',
      '#maxlength' => 255,
      '#required' => True,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Valider'),
    ];
    // Add delete button only for edit page.
    if($this->operation == "edit"){
      $form['actions']['Delete'] = [
        '#type' => 'submit',
        '#weight' => 999,
        '#value' => $this->t('Delete'),
        '#submit' => ['::redirectFormDelete'],
        '#attributes' => array(
          'class' => array('button', 'button--danger'),
        ),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submit form action add new line and show status message
    $messenger = \Drupal::messenger();
    $field = $form_state->getValues();
    $fields = [
      'event_name' => Xss::filterAdmin($field['event_name']),
      'event_description' => Xss::filterAdmin($field['event_description']),
      'event_salon' => Xss::filterAdmin($field['event_salon']),
      'event_ecole' => $field['ecole'],
    ];
    if($this->operation == "add") {
      $this->database->InserNewLineJpo($fields);
      $messenger->addStatus("Votre événement a été enregistré à jour avec succés!");

    }else{
      $this->database->updateLineJpo($fields, $this->id);
      $messenger->addStatus("Votre événement a été mis à jour avec succés!");
    }
  }
  /**
   * {@inheritdoc}
   */
  public function redirectFormDelete(array &$form, FormStateInterface $form_state){
    $url = Url::fromRoute('cspw_jpo_event_form.jpo_delete', ['id' => $this->id]);
    return $form_state->setRedirectUrl($url);
  }

}
