<?php

namespace Drupal\fdj_content\Normalizer;

use Drupal\serialization\Normalizer\ContentEntityNormalizer;
use Drupal\node\NodeInterface;
use Drupal\books\Entity\BookEntity;

/**
 * Converts the Drupal entity object structures to a normalized array.
 */
class FDJPageNodeEntityNormalizer extends ContentEntityNormalizer {
  /**
   * The interface or class that this Normalizer supports.
   * * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\node\NodeInterface';

  /** * {@inheritdoc} */
  public function supportsNormalization($data, $format = NULL) {
  // If we aren't dealing with an object or the format is not supported return// now.
    if (!is_object($data) || !$this->checkFormat($format)) {
  return FALSE;
}
  // This custom normalizer should be supported for "fdj_page" nodes if includes variable in url.
  //if ($data instanceof NodeInterface && $data->getType() == 'fdj_page' && isset($_GET['includes']) && $_GET['includes'] == "results") {
  if ($data instanceof BookEntity) {
    return TRUE;
}
// Otherwise, this normalizer does not support the $data object.
return FALSE;
}

/**
 * {@inheritdoc}
 */
public function normalize($entity, $format = NULL, array $context = array()) {
  $attributes = parent::normalize($entity, $format, $context);
  $url_game = "";
  $text_link= "";
  // get list result.
  /*$list = $attributes['field_result_list'];
  foreach ($list as $key => $value){
    $node = $this->entityTypeManager->getStorage('node')->load($value['target_id']);
    $img_id = $node->get('field_logo')->getValue()[0]['target_id'];
    if($node->get('field_game_link')->getValue()){
      $url_game = $node->get('field_game_link')->getValue()[0]['uri'];
      $text_link = $node->get('field_game_link')->getValue()[0]['title'];
    }
    $file = \Drupal\file\Entity\File::load($img_id);
    $path_img = $file->getFileUri();
    // Set new lest result
    $list[$key] = [
                     'nid' => $node->id(),
                     'title' => $node->getTitle(),
                     'img_url'=> file_create_url($path_img),
                     'link' => $url_game,
                     'text_link' => $text_link
                   ];
  }*/
  $list = [
    'nid' => "aaa",
    'title' => "bbb",
    'img_url'=> "ccc",
    'link' => "ddd",
    'text_link' => "fff"
  ];
  $attributes['term'] = $list;
  return $attributes;
}
}
