<?php

namespace Drupal\fdj_content\Event;

use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class UserBetEvent extends Event
{

  const EVENT_NAME = 'mespronos_user_bet';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;


  public function __construct(UserInterface $account)
  {
    $this->account = $account;
  }

}
