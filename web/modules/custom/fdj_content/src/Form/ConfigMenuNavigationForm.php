<?php

namespace Drupal\fdj_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config Menu Navigation form.
 */
class ConfigMenuNavigationForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'menu_navigation';
  }

  /**
   * Form build.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('c4com_content.principal_menu.settings');
    $form['url_redirection'] = [
      '#type' => 'fieldset',
      '#title' => t('Principal menu redirection'),
      '#weight' => 1,
    ];

    $form['url_MAP'] = [
      '#type' => 'fieldset',
      '#title' => t('MAP'),
      '#weight' => 2,
    ];
    $form['url_logo_config'] = [
      '#type' => 'url',
      '#title' => $this->t('URL logo Carrefour'),
      '#default_value' => $config->get('url_logo_config'),
      '#weight' => 0,
    ];

    $form['url_redirection']['label_url_redirection'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 20,
      '#default_value' => $config->get('label_url_redirection'),
      '#weight' => 0,
    ];

    $form['url_redirection']['link_url_redirection'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('link_url_redirection'),
      '#weight' => 1,
    ];

    $form['url_redirection']['url_redirection_open_new_onglet'] = [
      '#type' => 'select',
      '#title' => $this->t('Ouvrir dans un nouvel onglet'),
      '#default_value' => $config->get('url_redirection_open_new_onglet'),
      '#options' => [
        '_self' => $this->t('Open in the same tab'),
        '_blank' => $this->t('Open in a new tab'),
      ],
      '#weight' => 2,
    ];

    $form['url_MAP']['url_redirection_map'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('url_redirection_map'),
      '#weight' => 0,
    ];

    $form['url_MAP']['url_redirection_map_open_new_onglet'] = [
      '#type' => 'select',
      '#title' => $this->t('Ouvrir dans un nouvel onglet'),
      '#default_value' => $config->get('url_redirection_map_open_new_onglet'),
      '#options' => [
        '_self' => $this->t('Open in the same tab'),
        '_blank' => $this->t('Open in a new tab'),
      ],
      '#weight' => 1,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 4,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('c4com_content.principal_menu.settings')
      ->set('url_logo_config', $form_state->getValue('url_logo_config'))
      ->set('label_url_redirection', $form_state->getValue('label_url_redirection'))
      ->set('link_url_redirection', $form_state->getValue('link_url_redirection'))
      ->set('url_redirection_open_new_onglet', $form_state->getValue('url_redirection_open_new_onglet'))
      ->set('url_redirection_map_open_new_onglet', $form_state->getValue('url_redirection_map_open_new_onglet'))
      ->set('url_redirection_map', $form_state->getValue('url_redirection_map'))
      ->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['c4com_content.principal_menu.settings'];
  }

}
