<?php

namespace Drupal\fdj_content\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Config Menu Navigation form.
 */
class ConfigWS extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ws_config';
  }

  /**
   * Form build.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {


    $form['email'] = [
      '#type' => 'horizontal_tabs',
      '#title' => $this->t('Emails'),
    ];
    // These email tokens are shared for all settings, so just define
    // the list once to help ensure they stay in sync.


    $items = array(
      array(
        'nid' => 1,
        'name' => 'Item 1'
      ),
      array(
        'nid' => 2,
        'name' => 'Item 2'
      ),
      array(
        'nid' => 3,
        'name' => 'Item 3'
      ),
      array(
        'nid' => 4,
        'name' => 'Item 4'
      ),
    );
    $url = Url::fromUri('http://www.example.com/');
    $external_link = \Drupal::l(t('External link'), $url);
    $counter = 1;
    foreach ($items as $item) {
      $form['email_admin_created'.$counter] = [
        '#type' => 'details',
        '#title' => "xxxx",
        '#group' => 'email',

      ];
      $form['email_admin_created'.$counter]['user_mail_register_admin_created_subject'.$counter] = [
        '#type' => 'textfield',
        '#title' => $this->t('Subject'),
        '#maxlength' => 180,
      ];
      $form['email_admin_created'.$counter]['user_mail_register_admin_created_body'.$counter] = [
        '#type' => 'textarea',
        '#title' => $this->t('Body'),
        '#rows' => 15,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
        '#weight' => 4,
      ];

    $counter++;
    }



    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['c4com_content.ws_config.settings'];
  }

}
