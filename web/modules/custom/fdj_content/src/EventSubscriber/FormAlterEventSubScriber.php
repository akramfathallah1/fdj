<?php

namespace Drupal\fdj_content\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class FormAlterEventSubScriber.
 */
class FormAlterEventSubScriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HookEventDispatcherInterface::FORM_ALTER => 'hookFormAlter',
    ];
  }

  /**
   * Altering form array from here.
   */
  public function hookFormAlter($event) {
    $form_id = $event->getFormId();
      // Custom validation for color palette.
      if ($form_id == 'node_fdj_result_form') {
        $form = $event->getForm();
        $form['#validate'][] = [$this, 'validateFormResult'];
        $event->setForm($form);
      }
  }

  /**
   * Validate Form result function.
   */
  public function validateFormResult(array &$form, FormStateInterface $form_state) {
    $jeu = $form_state->getValue('field_fdj_jeu')[0]['value'];
    if(($jeu ==='loto' || $jeu ==='euromillions') && empty($form_state->getValue('field_game_link')[0]['uri'])){
      $form_state->setErrorByName('field_game_link', "LIEN DU JEU est requis pour lex jeux Loto et Euromillions");
    }

  }

}
