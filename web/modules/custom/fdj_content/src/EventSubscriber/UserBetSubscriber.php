<?php

namespace Drupal\fdj_content\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\fdj_content\Event\UserBetEvent;

class UserBetSubscriber implements EventSubscriberInterface {

  public function onBet(UserBetEvent $event) {
    // On peut récuperer les attributs de l'évènement
    $user = $event->account;
   // kint($event);
    // À vous de faire ce que vous voulez ici
  }

  public static function getSubscribedEvents() {
    // Définition du ou des évènements que l'on écoute et méthode à executer
    return [
      UserBetEvent::EVENT_NAME => 'onBet',
    ];
  }

}
