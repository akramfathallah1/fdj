<?php

/**
 * Get deployed app version.
 */
class Version {

  /**
   * Method to get last git app tag.
   */
  public static function get() {
    $version = trim(exec('git describe --tags --abbrev=0'));
    return $version;
  }

}
