<?php

namespace Drupal\Composer\Plugins;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * Take ability to manage fresh installation of Drupal instance with configuration.
 */
class DrupalInstall extends DrupalHandlerBase {

  /**
   * Execute all process commands needed by Drupal fresh installation.
   *
   * These script assume all syncronization of drupal configuration.
   *
   * @param Event $event
   *
   * @throws \Symfony\Component\Process\Exception\ProcessFailedException
   */
  public static function process(Event $event) {
    $fs = new Filesystem();
    $io = $event->getIO();
    $step = 1;

    $io->write("
    ######## ########        ## 
    ##       ##     ##       ## 
    ##       ##     ##       ## 
    ######   ##     ##       ## 
    ##       ##     ## ##    ## 
    ##       ##     ## ##    ## 
    ##       ########   ###### 
    _______ INSTALLING _________
    ");
    $io->write("<info>#step {$step}.</info> Settings : Prepare directories");
    self::prepareFilesDirectories($event);

    $params = self::getDrushConfig(file_get_contents('app/Drupal/parameters.yml'));
    $site_params = $params['parameters'];
    $dev_modules = $params['parameters']['dev.modules'];
    $step++;

    // Drupal site install.
    $io->write("");

    $io->write("<info>#step {$step}.</info> Drupal install : Site install");
    // Now we need to change permission of this file before re-install.
    $fs->chmod(static::getDrupalRootFolder(getcwd()) . '/sites/default/settings.php', 0666);
    $process = new Process(self::drush() . " si standard --site-name='FDJ' --account-name='admin' --account-pass='admin' --account-mail='akramfathallah1@gmail.com' --locale='en' -y");
    $process->setTimeout('30000');
    $process->run();

    if (!$process->isSuccessful()) {
      throw new ProcessFailedException($process);
    }

    self::writeDrushOutput($io, $process);
    $step++;

      // Drupal cache rebuild.
      $io->write("");

      $io->write("<info>#step {$step}.</info> Drupal install : Clear Rebuild");
      $process = new Process(self::drush() . ' cr');
      $process->setTimeout('30000');
      $process->run();

      if (!$process->isSuccessful()) {
          throw new ProcessFailedException($process);
      }

      self::writeDrushOutput($io, $process);
      $step++;
      // Enable custom modules.
      $io->write("");
      $io->write("<info>#step {$step}.</info> Dev modules : Enable custom modules.");
      $custom_modules = 'fdj_content';
      $process = new Process(self::drush() . ' en ' . $custom_modules . ' -y');
      $process->setTimeout('30000');
      $process->run();
      if (!$process->isSuccessful()) {
          throw new ProcessFailedException($process);
      }

      self::writeDrushOutput($io, $process);
      $step++;

      // Enable custom features.
      $io->write("");
      $io->write("<info>#step {$step}.</info> Dev modules : Enable custom features.");
      $custom_modules = 'fdj_ft_content';
      $process = new Process(self::drush() . ' en ' . $custom_modules . ' -y');
      $process->setTimeout('30000');
      $process->run();
      if (!$process->isSuccessful()) {
          throw new ProcessFailedException($process);
      }

      self::writeDrushOutput($io, $process);
      $step++;
    $io->write("<info>#step {$step}. settings.local permissions</info>");
    $fs->chmod(self::getDrupalRootFolder(getcwd()) . "/sites/default/settings.local.php", 0666);
    $fs->chmod(self::getDrupalRootFolder(getcwd()) . "/sites/default/files/", 0777);
    $io->write("* Update `<info>sites/default/settings.local.php</info>` file with chmod 0666");
    $io->write("
    ███████╗██╗   ██╗ ██████╗ ██████╗███████╗███████╗███████╗
    ██╔════╝██║   ██║██╔════╝██╔════╝██╔════╝██╔════╝██╔════╝
    ███████╗██║   ██║██║     ██║     █████╗  ███████╗███████╗
    ╚════██║██║   ██║██║     ██║     ██╔══╝  ╚════██║╚════██║
    ███████║╚██████╔╝╚██████╗╚██████╗███████╗███████║███████║
    ╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝╚══════╝╚══════╝╚══════╝
    ");
  }

}
