import React, {Component} from 'react';
import axios from 'axios'

export default class EntityResult extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.wssource = "http://localhost:8000/";
    this.uuid = this.props.match.params.id;
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.getResultDetails(this.props.match.params.id)
  }


  //Function to Load the Resultdetails data from json.
  getResultDetails(id) {
    axios.get('http://localhost:8000/apijson/fdj_result/'+id).then(response => {
      this.setState({result: response.data})
    })
  };

  render (){
   var jeu, title, img_url, game_link, link_text = "";
      var link = "Pas de lien pour ce jeu";
       if(this.state.result) {
           jeu = this.state.result.data.field_fdj_jeu[0].value;
           title = this.state.result.data.title[0].value;
           img_url = this.state.result.data.field_logo[0].url;
           if (this.state.result.data.field_game_link[0].uri) {
               game_link = this.state.result.data.field_game_link[0].uri;
               link_text = this.state.result.data.field_game_link[0].title;
               link = <a href={game_link} className='indent'>{link_text}</a>;
           }
       }
      return (<div className="result-list">
        <h1>Détails du résultat : </h1>
           <p>Titre : {title}</p>
           <p>Jeu : {jeu}</p>
           logo : <img src={img_url}  alt={title} width={40} /> <br/>
           Lien du jeu : {link}

      </div>)
    }
}
