import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Results from './Results'
import Pages from './Pages'
import NotFoundPage from './NotFoundPage'
import EntityResult from './EntityResult'
import EntityPage from './EntityPage'

import { BrowserRouter as Router, Switch , Route, Redirect, Link} from 'react-router-dom';


class App extends Component {
    render() {
    console.log("Host URL"+process.env.PUBLIC_URL);

      return (
      <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">App FDJ</h1>

        </header>
            <div className="container">
                <ul className="menu">
                    <li><Link to={'/'} className="nav-link"> Accueil </Link></li>
                    <li><Link to={'/results'} className="nav-link">Resultats</Link></li>
                    <li><Link to={'/pages'} className="nav-link">Pages</Link></li>
                </ul>
            </div>
          <Switch>
                <Route exact path= "/" render={() => (
                  <Redirect to="/results"/>
                )}/>
                 <Route exact path='/results' component={Results} />
                 <Route exact path='/pages' component={Pages} />
                 <Route exact path='/resultat/:id'  component={EntityResult} />
                 <Route exact path='/page/:id'  component={EntityPage} />
              <Route path="/404" component={NotFoundPage} />
              <Redirect to="/404" />
          </Switch>
      </div>
    </Router>
    );
  }
}

export default App;
