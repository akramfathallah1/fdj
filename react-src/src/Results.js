import React, {Component} from 'react';
import axios from 'axios'
import {Link} from "react-router-dom";

export default class Results extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.allResult()
  }
  //Function to Load the Resultdetails data from json.
  allResult() {

    axios.get('http://localhost:8000/apijson/list/fdj_result').then(response => {
      this.setState({results: response.data.data})
    })
  };
  render (){

    if (!this.state.results){
      return (<p>Loading Data</p>)
    }else {
      var indents = [];
      this.state.results.forEach(function (item) {
        indents.push(<li key={item.nid[0].value}><Link to={/resultat/ + item.nid[0].value} className='indent'
                                             key={item.id}>{item.title[0].value}</Link></li>);
      });
      if(indents.length){
        return (<div className="result-list">
        <h1>Liste des resultats : </h1>
        <ul>
          {indents}
        </ul>
       </div>)
    }else{
        return (<div className="result-list"><p>Pas de resultat</p>
        </div>)
      }
    }
  }
}
