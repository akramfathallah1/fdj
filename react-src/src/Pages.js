import React, {Component} from 'react';
import axios from 'axios'
import {Link} from "react-router-dom";

export default class pages extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.allResult()
  }
  //Function to Load the pagedetails data from json.
  allResult() {
    var url_ws = "http://localhost:8000/apijson/list/fdj_page";
    var url =  window.location.href;
    var url = new URL(url);
    var param = url.searchParams.get("includes");
    if(param == "results"){
      url_ws = "http://localhost:8000/apijson/list/fdj_page?includes=results"
    }
    axios.get(url_ws).then(response => {
      this.setState({pages: response.data.data})
    })
  };
  render (){

    if (!this.state.pages){
      return (<p>Loading Data</p>)
    }else{
      var pages = [];
      var result = [];
      this.state.pages.forEach(function(item){
        pages.push(<li key={item.nid[0].value}><Link to={/page/+item.nid[0].value}  className='indent' >{item.title[0].value}</Link><br/> Json result :{JSON.stringify(item.field_result_list)}</li>);
      });
     if(pages.length){
      return (<div className="result-list">
        <h1>Liste des pages : </h1>
        <ul>
          {pages}
        </ul>
      </div>)
     }else{
       return (<div className="<no-result-list>"><p>Pas de resultat</p>
       </div>)
     }
    }
  }
}
